# Syndictive

![Syndictive icon](static/icon.svg)

An application that automatically scrapes webpages and converts them to Atom feeds, so that you can use
your favorite feed reader and view all your content in one place.

Feeds are automatically updated about once an hour.

## Warning

This uses Flask, but it's not a web application. There is no security whatsoever!
Don't run it as a web server or you'll be sorry.

## Disclaimer

Syndictive is definitely not finished yet; there may be bugs, performance issues, and all kinds of other problems.

## Quickstart

Ensure that you have Chromedriver and Python >=3.10 installed.

Then, in a terminal or command line:

```bash
python3 -m pip install --user -r requirements.txt
python3 main.py
```

Once Syndictive is running, open a browser and visit http://localhost:8080.

## Hosting on LAN

Be careful when doing this, as Syndictive has no authentication!

Add `server_name` to config.json; it should contain the local IP address of the server.
This will prevent OPML exports and similar from using hardcoded localhost URLs.

## Configuring arbitrary plugin headers

* `*_headers` - Where `*` is the name of the plugin. An object containing
  HTTP headers to be injected into plugin requests.
  This only applies for `JSONPlugin` and `SoupPlugin` instances.
  You can use this functionality to e.g. change user agents.

## Configuring MangaDex plugin

* `mangadex_language` - A string representing the language that you want chapters to be received in.
  Defaults to `"en"`.

## Configuring Madara plugin

* `madara_date_formats` - Simple object containing (hostname, date format) mappings.
  Add this to config.json to override auto-detection of date formats.

## Usage tips

Running Syndictive with the `-v` argument shows debugging output.

You can add the `/add-source` endpoint as a search engine to your browser and assign it a keyword.
This allows you to quickly add any URL to Syndictive from your browser interface.
The URL format should be `http://localhost:%P/add-source?url=%S` where `%P` is the port number and `%S` is the search term.

A specific plugin can be forced by using `http://localhost:%P/add-source?plugin=%N&url=%S`,
where `%N` is the name of the plugin.

## Akregator integration

Syndictive can automatically export all feeds to Akregator.
To do so, set `akregator_integration` in config.json to `true`.

## Todo

* Have a default set of pre-populated sources
* Add authentication so that Syndictive can be used as a web server

"""Syndictive (originally mangadex2rss) feed server.

Periodically fetches pages and converts them to Atom (or RSS).
"""

from datetime import datetime
from hashlib import sha256
import logging
from multiprocessing import Process, Manager
import os
from random import random, shuffle
import subprocess
from time import sleep
import traceback
from urllib.parse import urlparse
from xml.sax.saxutils import quoteattr

from apscheduler.schedulers.background import BackgroundScheduler
try:
    import dbus
except Exception:
    dbus = None
from flask import Flask, Response, abort, flash, jsonify, make_response, redirect, render_template, request, url_for
from func_timeout import func_timeout
from helium import kill_browser
try:
    from plyer import notification
except Exception:
    notification = None
import requests
from werkzeug.utils import safe_join

import syndictive
from syndictive.datastructures import Source

APPLICATION_NAME = "Syndictive"
REQUIRES_FEED_REBUILD = "Requires Feed Rebuild"

logger = syndictive.helpers.create_logger(__name__)
log = logging.getLogger("werkzeug")
log.setLevel(logging.ERROR)


if notification and dbus:
    def notify(*args):
        """Send a system notification."""
        try:
            notification.notify(*args)
        except Exception:
            print(*args)
            logger.warning(traceback.format_exc())
else:
    def notify(*args):
        """Send a system notification."""
        try:
            subprocess.run(("notify-send",) + args, check=True, timeout=1)
        except Exception:
            print(*args)
            logger.warning(traceback.format_exc())


def process_source(config: dict, db: syndictive.JsonDBManager, previews: syndictive.LMDBManager,
                   source_id: str, *, finally_kill_browser: bool = True):
    """Save one source page to the database."""

    source_data = db.get_source(source_id)
    if not source_data:
        source_data_legacy = config["sources"][source_id]
        if isinstance(source_data_legacy, str):
            plugin_name = syndictive.get_plugin_for_source(source_data_legacy).name
            source_data = Source(
                url=source_data_legacy,
                plugin=plugin_name
            )
        elif isinstance(source_data_legacy, dict):
            source_data = Source(
                url=source_data_legacy.get("url"),
                plugin=source_data_legacy.get("plugin")
            )
        else:
            raise Exception(f"Config for source {source_id} is corrupt or invalid!")

    last_checked = datetime.now()
    source_url = source_data.url
    plugin_name = source_data.plugin

    try:
        logger.info(f"Building feed for {source_url}...")

        if config.get("get_preview_images"):
            preview_exists = previews.get(source_id)
            if not preview_exists:
                get_preview = True
            else:
                get_preview = False
        else:
            get_preview = False

        plugin = syndictive.plugins[plugin_name]
        context = syndictive.PluginContext(
            source_url=source_url,
            config=config,
            preview_requested=get_preview,
            title=source_data.title
        )

        for attempt in range(0, 3):
            try:
                feed = plugin.run(context)
            except Exception as error:
                if attempt == 2:
                    raise error
                random_wait = random() * 2 + 1
                logger.warning(f"An error occurred fetching {source_url}; retrying in {random_wait} seconds...")
                logger.warning(traceback.format_exc())
                sleep(random_wait)
            else:
                break
        # Update source URL if modified in context
        if context.source_url != source_data.url:
            source_data.url = context.source_url
        if context.rss:
            feed_contents = feed.to_rss(image_url=f"/preview/{source_id}")
        else:
            feed_contents = feed.to_atom(image_url=f"/preview/{source_id}")
        try:
            num_items = len(feed.items)
        except Exception:
            try:
                if context.rss:
                    num_items = feed_contents.count("<item>")
                else:
                    num_items = feed_contents.count("<entry>")
            except Exception:
                num_items = 0

        source_data.num_items = num_items

        if not preview_exists:
            if feed.preview:
                previews.put(source_id, feed.preview)
            else:
                logger.error("Failed to get preview!")
        if context.rss:
            source_data.rss = feed_contents
            source_data.atom = None
        else:
            source_data.rss = None
            source_data.atom = feed_contents

    except Exception as error:
        logger.error("An error ocurred while building:")
        logger.error(traceback.format_exc())
        source_data.error = str(error)

    else:
        source_data.error = None
        if not source_data.num_items:
            source_data.error = "Feed is empty!"
        logger.info(f"Finished building feed for {source_url}")
        for element in ("title", "description"):
            try:
                setattr(source_data, element, getattr(feed, element))
            except Exception:
                logger.error(traceback.format_exc())

    finally:
        source_data.last_checked = last_checked
        db.put_source(source_id, source_data)
        if finally_kill_browser:
            logger.info("Closing browser instance.")
            try:
                kill_browser()
            except Exception:
                pass


def _process_sources(config: dict, db: syndictive.JsonDBManager, previews: syndictive.LMDBManager,
                     source_ids: list[str]):
    """Poll pages for sources, convert them to Atom/RSS feeds, and save the results to files.

    This should NOT be run as-is.
    """

    try:
        for index, source_id in enumerate(source_ids, start=1):
            if index > 1:
                sleep(random() * 2)
            logger.info(f"{index} of {len(source_ids)}:")
            try:
                func_timeout(
                    30,
                    process_source,
                    args=(config, db, previews, source_id),
                    kwargs={"finally_kill_browser": False}
                )
            except Exception:
                logger.error(traceback.format_exc())
        logger.info("Process has finished building feeds.")
    except Exception:
        logger.error(traceback.format_exc())
    finally:
        logger.info("Closing browser instance.")
        try:
            kill_browser()
        except Exception:
            pass


def process_sources_worker(semaphore: list, db: syndictive.JsonDBManager, previews: syndictive.LMDBManager):
    """Run the above in separate processes, in its own worker process."""

    while True:
        try:
            config = semaphore[0]
        except Exception:
            config = False
        if not config:
            sleep(1)
            continue
        try:
            notify(APPLICATION_NAME, "Rebuilding feeds...")

            start = datetime.utcnow()

            processes = []
            num_processes = config["num_processes"]

            source_ids = list(config["sources"].keys())
            shuffle(source_ids)
            chunk_size = (len(source_ids) // num_processes)
            if not len(source_ids) % num_processes == 0:
                chunk_size += 1
            for process_number, index in enumerate(range(0, len(source_ids), chunk_size), start=1):
                if process_number > 1:
                    sleep(random() * 2)
                processes.append(Process(target=_process_sources,
                                         args=(config, db, previews,
                                               source_ids[index:index+chunk_size])))
                processes[-1].start()

            for process in processes:
                process.join()

            db.save()

            if config.get("akregator_integration"):
                logger.info("Requesting Akregator feed fetch...")
                if dbus:
                    try:
                        akregator_dbus_interface = dbus.SessionBus().get_object("org.kde.akregator", "/Akregator")
                        akregator_dbus_interface.fetchAllFeeds()
                    except Exception:
                        logger.error(traceback.format_exc())
                elif config.get("port_number_dbus"):
                    try:
                        response = requests.post(
                            f"http://localhost:{config['port_number_dbus']}/fetchAllFeeds",
                            timeout=5
                        )
                        logger.info(response.text)
                    except Exception:
                        logger.error(traceback.format_exc())

            end = datetime.utcnow()
            time_elapsed = str(end - start).split(".", 1)[0]

            message = f"Finished building all feeds ({time_elapsed})."
            notify(APPLICATION_NAME, message)
            logger.info(message)

        except Exception:
            notify(APPLICATION_NAME, "Could not rebuild feeds!")
            logger.error(traceback.format_exc())

        semaphore[0] = False


def get_source_id(source_url: str):
    """Given a source URL, generate a somewhat useful and unique ID."""
    return sha256(source_url.rstrip("/").encode("utf-8")).hexdigest()


def create_app(config: syndictive.ConfigManager, db: syndictive.JsonDBManager,
               previews: syndictive.LMDBManager):
    """Flask application factory."""

    app = Flask(__name__, template_folder="templates", static_folder="static")
    app.secret_key = os.urandom(64)

    if config.get("server_name"):
        app.config["SERVER_NAME"] = f"{config['server_name']}:{config['port_number']}"
        url_base = f"http://{app.config['SERVER_NAME']}"
    else:
        url_base = f"http://localhost:{config['port_number']}"

    manager = Manager()
    process_sources_semaphore = manager.list()
    process_sources_semaphore.append(False)
    process_sources_worker_process = Process(
        target=process_sources_worker,
        args=(process_sources_semaphore, db, previews)
    )
    process_sources_worker_process.start()

    def _rebuild_feeds():
        process_sources_semaphore[0] = config.deepcopy()

    scheduler = BackgroundScheduler()
    scheduler.add_job(func=_rebuild_feeds,
                      trigger="cron", hour="*", jitter=600)
    scheduler.start()

    @app.route("/")
    def index():
        """Index route. Displays a list of actions, plus all sources currently available."""

        search_terms = request.args.get("q")
        sources = config["sources"]
        if search_terms:
            search_terms = search_terms.lower().split()
            sources_copy = {}
            for source_id in sources:
                source_data = db.get_json(source_id)
                for value in source_data.values():
                    if all(term in str(value).lower() for term in search_terms):
                        try:
                            sources_copy[source_id] = Source.from_dict(source_data)
                        except Exception:
                            sources_copy[source_id] = None
                        break
        else:
            sources_copy = {}
            for source_id in sources:
                source_data = db.get_source(source_id)
                if not source_data:
                    source_data = Source(
                        url=sources[source_id],
                        title=REQUIRES_FEED_REBUILD,
                        plugin=""
                    )
                sources_copy[source_id] = source_data
        return render_template("index.jinja", plugin_names=syndictive.plugins.keys(),
                               sources=sources_copy, q=request.args.get("q", "").lower(),
                               text_only=config.get("text_only"),
                               akregator_integration=config.get("akregator_integration"))

    @app.route("/stats")
    def stats():
        """Info and stats regarding the Syndictive instance."""

        total_sources = len(config["sources"])
        sources_by_plugin = {}
        warnings = {}
        for source_id in config["sources"]:
            source_data = db.get_source(source_id)
            if not source_data:
                warnings[source_id] = f"{source_id} ({REQUIRES_FEED_REBUILD})"
                continue
            plugin_name = source_data.plugin or "Unknown plugin"
            sources_by_plugin.setdefault(plugin_name, 0)
            sources_by_plugin[plugin_name] += 1
            if source_data.error or not source_data.num_items:
                warnings[source_id] = (
                    f"{source_data.title or source_data.url or source_id} "
                    f"({source_data.error or 'Feed is empty'})"
                )
        return render_template("stats.jinja",
                               plugin_names=syndictive.plugins.keys(),
                               sources_by_plugin=sources_by_plugin,
                               total_sources=total_sources,
                               warnings=warnings,
                               akregator_integration=config.get("akregator_integration"))

    @app.route("/opml")
    def opml():
        """Generate an OPML file of available sources."""

        logger.info("Exporting to OPML...")
        opml_items_by_domain = {}
        for source_id in tuple(config["sources"].keys()):
            source_data = db.get_source(source_id)
            if not source_data:
                continue
            try:
                feed_url = f"{url_base}{url_for('feed', source_id=source_id)}"
                source_url_domain = quoteattr(urlparse(source_data.url).netloc)
                opml_items_by_domain.setdefault(source_url_domain, {})
                opml_items_by_domain[source_url_domain][feed_url] = quoteattr(source_data.title)
            except Exception:
                continue
        opml_xml = render_template("opml.jinja", opml_items_by_domain=opml_items_by_domain)
        response = make_response(opml_xml)
        response.headers["Content-Type"] = "text/x-opml"
        response.headers["Content-Disposition"] = "attachment; filename=syndictive.opml"
        return response

    @app.route("/semaphore", methods=["GET", "DELETE"])
    def semaphore():
        """Clear semaphore for processing sources."""
        process_sources_semaphore[0] = False
        message = "Cleared processing sources semaphore."
        if request.method == "GET":
            flash(message)
            return redirect(url_for("index"))
        return message

    def _akregator():
        if not config.get("akregator_integration"):
            abort(404)

        logger.info("Preparing to send sources to Akregator...")

        filepath_akregator_export = safe_join(config["tmp_directory"], "akregator.opml")
        url_filepath_export = f"file://{filepath_akregator_export}"
        try:
            if dbus:
                akregator_dbus_interface = dbus.SessionBus().get_object("org.kde.akregator", "/Akregator")
                akregator_dbus_interface.exportFile(url_filepath_export)
            elif config.get("port_number_dbus"):
                response = requests.post(
                    f"http://localhost:{config['port_number_dbus']}/exportFile",
                    json={"url": url_filepath_export},
                    timeout=5
                )
                logger.info(response.text)
        except Exception:
            logger.error(traceback.format_exc())
            flash(
                "Akgregator integration failed! Make sure that Akregator is running."
            )
            return redirect(url_for("index"))
        with open(filepath_akregator_export, encoding="utf-8") as file_object:
            akregator_export_contents = file_object.read()
        logger.info("Checking which feeds are already present in Akregator...")
        sorted_feed_urls = {}
        for source_id in tuple(config["sources"].keys()):
            feed_url = f"{url_base}{url_for('feed', source_id=source_id)}"
            if quoteattr(feed_url) in akregator_export_contents:
                continue
            source_data = db.get_source(source_id)
            if not source_data:
                continue
            try:
                sorting_key = str(source_data.title)[0].upper()
            except Exception:
                sorting_key = "0"
            sorted_feed_urls.setdefault(sorting_key, [])
            sorted_feed_urls[sorting_key].append(feed_url)
        logger.info("Sending sources to Akregator...")
        errors = 0
        skipped = 0
        feed_urls_added = 0
        for sorting_key, feed_urls in sorted(sorted_feed_urls.items(), key=lambda i: i[0]):
            if feed_urls_added >= 50:
                logger.warning("Skipping feeds to avoid crashing Akregator.")
                skipped += len(feed_urls)
                continue
            try:
                if dbus:
                    akregator_dbus_interface.addFeedsToGroup(feed_urls, sorting_key)
                elif config.get("port_number_dbus"):
                    response = requests.post(
                        f"http://localhost:{config['port_number_dbus']}/addFeedsToGroup",
                        json={"lst": feed_urls, "feedname": sorting_key},
                        timeout=5
                    )
                    logger.info(response.text)
            except Exception:
                logger.error("Failed! Please try again later.")
                logger.error(traceback.format_exc())
                errors += len(feed_urls)
            feed_urls_added += len(feed_urls)
        message = "Finished exporting to Akregator."
        logger.info(message)
        flash(message)
        if errors:
            error_message = f"{errors} sources were not correctly received by Akregator. Please try again later."
            logger.warning(error_message)
            flash(error_message)
        if skipped:
            skipped_message = (f"{skipped} sources were skipped to avoid crashing Akregator. "
                               "It is recommended to close and restart Akregator before continuing.")
            logger.warning(skipped_message)
            flash(skipped_message)
        return redirect(url_for("index"))

    @app.route("/akregator")
    def akregator():
        """Akregator integration."""
        return _akregator()

    @app.route("/config", methods=["GET", "POST"])
    def config_():
        """Get or reload configuration."""

        if request.method == "GET":
            logger.info("Config requested.")
            return jsonify(config)
        message = ""
        status = 200
        try:
            config.load()
            message = "Config reloaded."
        except AssertionError:
            message = "Config could not be loaded because it failed validation."
            logger.error(message)
            status = 500
        except Exception as error:
            message = f"Config could not be loaded: {error}."
            logger.error(traceback.format_exc())
            status = 500
        notify(APPLICATION_NAME, message)
        flash(message)
        return message, status

    @app.route("/add-source", methods=["POST", "GET"])
    def add_source():
        """Add a source page."""

        logger.info("Source add requested.")
        message = ""
        source_id = None

        if request.method == "POST":
            source_url = request.form.get("url", "")
            plugin_name = request.form.get("plugin", "")
        else:
            source_url = request.args.get("url", "")
            plugin_name = request.args.get("plugin", "")
        source_url = source_url.strip()
        if not source_url:
            flash("No URL was provided.")
            return redirect(url_for("index"))
        for source_id in tuple(config["sources"].keys()):
            source_data = db.get_source(source_id)
            if not source_data:
                continue
            if source_url in str(source_data.url):
                flash(f"{source_url} already exists (ID {source_id}) and was not added.")
                return redirect(url_for("source_", source_id=source_id))
        logger.info(f"Requested source URL is {source_url}")
        source_id = get_source_id(source_url)
        if not plugin_name:
            plugin = syndictive.get_plugin_for_source(source_url)
        else:
            plugin = syndictive.plugins.get(plugin_name)
        if not plugin:
            if plugin_name:
                message = f"Plugin \"{plugin_name}\" was not found."
            else:
                message = "No plugin for this source page was found."
            logger.error(message)
        elif source_id in config["sources"]:
            message = f"{source_url} already exists (ID {source_id}) and was not added."
            logger.error(message)
        else:
            message = f"{source_url} was added successfully."
            logger.info(f"Plugin {plugin.name} will be used.")
            source_data = Source(
                url=source_url,
                plugin=plugin.name
            )
            db.put_source(source_id, source_data)
            # Create a new process to create a new instance of Helium
            process = Process(target=process_source, args=(config.deepcopy(), db, previews, source_id))
            process.start()
            process.join()
            source_data = db.get_source(source_id)
            if isinstance(source_data, Source) and (source_data.atom or source_data.rss):
                config["sources"][source_id] = {
                    "url": source_url,
                    "plugin": plugin_name
                }
                try:
                    config.save()
                except Exception:
                    logger.error(traceback.format_exc())
                    logger.error("Config could not be saved!")
                db.save()
                logger.info(message)
            else:
                message = f"{source_url} was not added (probably invalid URL)"
        flash(message)
        notify(APPLICATION_NAME, message)
        if dbus and config.get("akregator_integration"):
            _akregator()
        if source_id in config["sources"]:
            return redirect(url_for("source_", source_id=source_id))
        return redirect(url_for("index"))

    @app.route("/rebuild-feeds", methods=["POST"])
    def rebuild_feeds():
        """Rebuild all feeds on demand. Normally, you should not have to do this."""

        if process_sources_semaphore[0]:
            message = "Feed rebuild is already in progress (or semaphore was not cleared)."
            status = 500
            logger.error(message)
        else:
            process_sources_semaphore[0] = config.deepcopy()
            message = "Feed rebuild requested."
            status = 200
            logger.info(message)
        flash(message)
        if status == 500:
            flash("If the problem persists, then clear the semaphore.")
        return message, status

    @app.route("/rebuild-feed/<source_id>", methods=["GET"])
    def rebuild_feed(source_id):
        """Rebuild Atom feed for an individual source."""

        if source_id not in config["sources"]:
            abort(404)
        source_data = db.get_source(source_id)
        if not source_data:
            abort(404)

        logger.info(f"Rebuilding feed for source {source_id}")

        # Create a new process, so as to create a new Helium instance
        process = Process(target=process_source, args=(config.deepcopy(), db, previews, source_id))
        process.start()
        process.join()

        message = "Source rebuild request was sent."
        flash(message)
        notify(APPLICATION_NAME, message)
        return redirect(url_for("source_", source_id=source_id))

    @app.route("/feed/<source_id>")
    def feed(source_id):
        """Atom or RSS feed for a given source.

        Atom is the default, but Syndictive also supports RSS.
        """

        if source_id not in config["sources"]:
            abort(404)
        source_data = db.get_source(source_id)
        if not source_data:
            abort(404)
        atom_contents = source_data.atom
        rss_contents = source_data.rss
        if not atom_contents and not rss_contents:
            abort(404)
        elif atom_contents:
            response = Response(atom_contents)
            response.headers["Content-Type"] = "application/atom+xml"
            response.headers["Content-Disposition"] = f"attachment; filename={source_id}.atom"
            return response
        elif rss_contents:
            response = Response(rss_contents)
            response.headers["Content-Type"] = "application/rss+xml"
            response.headers["Content-Disposition"] = f"attachment; filename={source_id}.rss"
            return response

    @app.route("/cleanup")
    def cleanup():
        """Delete orphaned database entries and fix config."""

        orphans_removed = 0
        config_fixed = 0
        for key in db.keys():
            if key not in config["sources"]:
                orphans_removed += 1
                del db[key]
                continue
            this_config_fixed = False
            try:
                if not config["sources"][key].get("plugin"):
                    config["sources"][key]["plugin"] = \
                        syndictive.get_plugin_for_source(config["sources"][key]["url"]).name
                    this_config_fixed = True
            except Exception:
                logger.warning(traceback.format_exc())
            try:
                source_data = db.get_source(key)
                if not source_data.plugin:
                    source_data.plugin = config["sources"][key]["plugin"]
                    db.put_source(key, source_data)
                else:
                    source_in_config = {
                        "url": source_data.url,
                        "plugin": source_data.plugin
                    }
                    if source_data.plugin and config["sources"][key] != source_in_config:
                        config["sources"][key] = source_in_config
                        this_config_fixed = True
            except Exception:
                logger.warning(traceback.format_exc())
            else:
                if this_config_fixed:
                    config_fixed += 1
        try:
            config.save()
        except Exception:
            logger.error(traceback.format_exc())
            logger.error("Config could not be saved!")
        db.save()
        for key in previews.keys():
            if key not in config["sources"]:
                orphans_removed += 1
                previews.delete(key)

        message_orphans = f"Trimmed {orphans_removed} orphaned database entries."
        logger.info(message_orphans)
        flash(message_orphans)

        message_fixed = f"Fixed {config_fixed} config entries."
        logger.info(message_fixed)
        flash(message_fixed)

        return redirect(url_for("index"))

    @app.route("/preview/<source_id>", methods=["GET", "DELETE"])
    def preview(source_id):
        """Preview for a given source."""

        if request.method == "DELETE":
            try:
                previews.delete(source_id)
                logger.info(f"Deleted preview for source {source_id}")
            except Exception:
                logger.info(f"Could not delete preview for source {source_id} (maybe it doesn't exist?)")
            return ""
        preview_contents = previews.get(source_id)
        if not preview_contents:
            return redirect(url_for("static", filename="blank-preview.png"))
        return Response(preview_contents, mimetype="image/png")

    @app.route("/source/<source_id>", methods=["POST", "GET", "DELETE"])
    def source_(source_id):
        """Minimal landing page for a given source."""

        if request.method == "DELETE":
            try:
                source_data = db.get_source(source_id)
                source_title = source_data.title
                source_url = source_data.url
                del config["sources"][source_id]
                del db[source_id]
                previews.delete(source_id)
                message = f"Deleted source {source_id}"
                if source_title:
                    message += f" ({source_title})"
                if source_url:
                    message += f" ({source_url})"
                logger.info(message)
            except Exception:
                logger.warning(f"Source {source_id} was already deleted?")

            try:
                config.save()
            except Exception:
                logger.error(traceback.format_exc())
                logger.error("Config could not be saved!")

            return ""
        source_data = db.get_source(source_id)
        if not source_data:
            try:
                source_data = Source(
                    url=config["sources"][source_id]["url"],
                    title=REQUIRES_FEED_REBUILD,
                    description=REQUIRES_FEED_REBUILD,
                    plugin=""
                )
            except Exception:
                abort(404)
        if request.method == "POST":
            if "url" in request.form:
                requested_url = request.form["url"].strip()
                if requested_url and requested_url != source_data.url:
                    try:
                        source_data.url = requested_url
                        config["sources"][source_id]["url"] = requested_url
                        try:
                            config.save()
                        except Exception:
                            logger.error(traceback.format_exc())
                            logger.error("Config could not be saved!")
                        flash(f"Updated source URL to {source_data.url} (will use on next rebuild)")
                    except Exception:
                        logger.error(traceback.format_exc())
                        flash("URL was not updated properly.")
            if "plugin" in request.form or "url" in request.form:
                try:
                    plugin_name = request.form.get("plugin")
                    if not plugin_name in syndictive.plugins:
                        plugin = syndictive.get_plugin_for_source(source_data.url)
                        plugin_name = plugin.name
                    source_data.plugin = plugin_name
                    config["sources"][source_id]["plugin"] = plugin_name
                    try:
                        config.save()
                    except Exception:
                        logger.error(traceback.format_exc())
                        logger.error("Config could not be saved!")
                    flash(f"Switched plugin to {plugin_name} (will use on next rebuild)")
                except Exception:
                    logger.error(traceback.format_exc())
                    flash("Plugin was not updated properly.")
            if request.form:
                db.put_source(source_id, source_data)
                db.save()
        source_plugin = syndictive.plugins.get(source_data.plugin)
        return render_template("source_info.jinja", plugin_names=syndictive.plugins.keys(),
                               source_id=source_id, source=source_data, source_plugin=source_plugin,
                               akregator_integration=config.get("akregator_integration"))

    return app


def main(config_filepath: str = "config.json", database_filepath: str = "sources.json",
         previews_dirpath: str = "previews.db", plugins_directory: str = "plugins"):
    """Main driver."""

    config_manager = syndictive.ConfigManager(filepath=config_filepath)
    config_manager.load()

    database_manager = syndictive.JsonDBManager(filepath=database_filepath)
    preview_manager = syndictive.LMDBManager(dirpath=previews_dirpath)

    if not os.path.isdir(config_manager["tmp_directory"]):
        if os.path.isfile(config_manager["tmp_directory"]):
            os.remove(config_manager["tmp_directory"])
        os.makedirs(config_manager["tmp_directory"])

    plugin_module_blocklist = config_manager.get("plugin_module_blocklist", [])
    syndictive.load_plugins_from_dir(plugins_directory, blocklist=plugin_module_blocklist)

    app = create_app(config=config_manager, db=database_manager, previews=preview_manager)
    app.config["TEMPLATES_AUTO_RELOAD"] = True
    app.run(port=config_manager["port_number"])


if __name__ == "__main__":
    main()

"""Syndictive helpers."""

import sys

from dateutil.parser import parse as parse_timestr
from jinja2 import Template
from logbook import Logger, StreamHandler
import parsedatetime

from . import defaults

_CALENDAR = parsedatetime.Calendar()
_IMG_TAG = Template('<img src="{{ url }}">')


def to_datetime(timestring):
    """Take a string and try to parse it automatically into a `datetime` object."""
    item_pubDate = None
    if "ago" in timestring:
        try:
            item_pubDate, _ = _CALENDAR.parseDT(timestring)
        except Exception:
            pass
    else:
        try:
            item_pubDate = parse_timestr(timestring)
        except Exception:
            try:
                item_pubDate, _ = _CALENDAR.parseDT(timestring)
            except Exception:
                pass
    return item_pubDate


def to_img(url):
    """Plonk a URL into an img tag."""
    return _IMG_TAG.render(url=url)


def create_logger(name):
    """Create a logger for a module."""
    info_handler = StreamHandler(sys.stdout, level="INFO")
    info_handler.formatter = defaults.FORMATTER
    info_handler.push_application()
    error_handler = StreamHandler(sys.stderr, level="WARNING")
    error_handler.formatter = defaults.FORMATTER
    error_handler.push_application()
    return Logger(name)

"""Feed datastructures. An abstracted Feed and FeedItem object are present here."""

from dataclasses import dataclass, field
from datetime import datetime

from dataclasses_json import dataclass_json, config
from feedgen.feed import FeedGenerator
from marshmallow import fields
import pytz
import rfeed

from . import helpers


@dataclass_json
@dataclass
class Source:
    """An abstracted source item."""

    url: str
    plugin: str
    num_items: int = 0
    error: str = None
    last_checked: datetime = field(
        default=datetime(1970, 1, 1),
        metadata=config(
            encoder=datetime.isoformat,
            decoder=datetime.fromisoformat,
            mm_field=fields.DateTime(format="iso")
        )
    )
    title: str = None
    description: str = None
    rss: str = None
    atom: str = None


@dataclass
class FeedItem:
    """An abstracted implementation of a feed item."""

    title: str = None
    link: str = None
    description: str = None
    publication_date: str = None
    author: str = None
    image: str = None
    guid: str = None

    def to_rss(self):
        """Convert to RSS."""
        if self.image:
            description = f"{self.description or ''}\n{helpers.to_img(self.image)}"
        else:
            description = self.description
        guid = self.guid or self.link
        return rfeed.Item(
            title=self.title,
            link=self.link,
            description=description,
            pubDate=self.publication_date,
            author=self.author,
            guid=rfeed.Guid(guid)
        )


@dataclass
class Feed:
    """An abstracted implementation of a feed."""

    title: str = None
    description: str = None
    source_url: str = None
    preview: bytes = None
    items: list[FeedItem] = None
    contents: str = None
    last_build_date: datetime = None
    language: str = None

    def to_atom(self, image_url: str = None):
        """Convert to Atom feed"""
        if self.contents:
            return self.contents
        fg = FeedGenerator()
        fg.title(self.title)
        fg.subtitle(self.description)
        fg.id(self.source_url)
        fg.link(href=self.source_url)
        fg.icon(image_url)
        fg.logo(image_url)
        try:
            fg.updated(self.last_build_date)
        except Exception:
            fg.updated(pytz.utc.localize(self.last_build_date))
        fg.language(self.language)
        items = self.items or []
        for item in items:
            entry = fg.add_entry()
            entry.title(item.title)
            entry.id(item.link)
            entry.link(href=item.link)
            if item.image:
                entry.content(f"{item.description or ''}\n{helpers.to_img(item.image)}")
            entry.summary(item.description)
            entry.author(name=item.author)
            try:
                entry.updated(item.publication_date)
            except Exception:
                entry.updated(pytz.utc.localize(item.publication_date))
        feed_str = fg.atom_str(pretty=True)
        if isinstance(feed_str, bytes):
            feed_str = feed_str.decode("utf-8")
        return feed_str

    def to_rss(self, image_url: str = None):
        """Convert to RSS."""
        if self.contents:
            return self.contents
        items = self.items or []
        if image_url:
            try:
                image = rfeed.Image(
                    title=self.title or self.source_url,
                    link=self.source_url,
                    url=image_url
                )
            except Exception:
                image = None
        else:
            image = None
        return rfeed.Feed(
            title=self.title,
            description=self.description,
            link=self.source_url,
            language=self.language,
            lastBuildDate=self.last_build_date,
            items=[i.to_rss() for i in items],
            image=image
        ).rss()

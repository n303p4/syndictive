"""Defaults for Syndictive."""

from datetime import datetime
import os

from werkzeug.utils import safe_join

def FORMATTER(record, _):
    """Default formatter for logging."""
    return f"[{datetime.now()}] p_{str(record.process)[-2:]} {record.level_name}: {record.message}"

USER_AGENT = (
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
    "AppleWebKit/537.36 (KHTML, like Gecko) "
    "Chrome/112.0.5615.137 Safari/537.36"
)
BROWSER_ARGUMENTS = [
    "--no-sandbox",
    "--disable-setuid-sandbox",
    "--disable-dev-shm-usage",
    "--disable-accelerated-2d-canvas",
    "--no-first-run",
    "--no-zygote",
    "--single-process",
    "--disable-gpu",
    "--FontRenderHinting[none]"
]

CONFIG = {
    "port_number": 8080,
    "sources": {},
    "tmp_directory": safe_join(os.path.expanduser("~"), ".syndictive"),
    "text_only": False,
    "num_processes": 2,
    "server_name": "",
    "madara_date_formats": {
        "www.mangaread.org": "%d.%m.%Y"
    },
    "get_preview_images": True,
    "akregator_integration": True
}

"""Class that handles a database.

The methods provided by this class are convenience methods.

If more performance is needed, the LMDB environment should be used directly.
"""

import json

import lmdb

from ..datastructures import Source
from .. import helpers

logger = helpers.create_logger(__name__)


class LMDBManager:
    """This object handles saving and loading of data to a database."""

    dirpaths = []

    def __init__(self, *, dirpath: str, map_size: int = 1000000000000):
        if dirpath in self.dirpaths:
            raise Exception(f"{dirpath} is already open in a manager!")
        self.dirpaths.append(dirpath)
        self.dirpath = dirpath
        self._environment = lmdb.open(self.dirpath, map_size=map_size)

    @property
    def environment(self):
        """Read-only pointer to the LMDB environment."""
        return self._environment

    def keys(self):
        """Get all keys in the database."""
        with self._environment.begin(buffers=True) as transaction:
            cursor = transaction.cursor()
            cursor.first()
            for key in cursor.iternext(values=False):
                try:
                    yield str(key, encoding="utf-8")
                except Exception:
                    yield bytes(key)

    def get(self, key: str, default = None):
        """Load raw bytes from database."""
        try:
            with self._environment.begin(buffers=True) as transaction:
                return bytes(transaction.get(bytes(key, encoding="utf-8")))
        except Exception:
            return default

    def put(self, key: str, value: bytes):
        """Save raw bytes to database."""
        with self._environment.begin(write=True, buffers=True) as transaction:
            transaction.put(bytes(key, encoding="utf-8"), value)

    def delete(self, key: str):
        """Delete a key from the database."""
        with self._environment.begin(write=True, buffers=True) as transaction:
            transaction.delete(bytes(key, encoding="utf-8"))

    def get_str(self, key: str, default = None):
        """Load string from database."""
        try:
            return str(self.get(key, default), encoding="utf-8")
        except Exception:
            return default

    def put_str(self, key: str, value: str):
        """Save string to database."""
        self.put(key, bytes(value, encoding="utf-8"))

    def get_json(self, key: str, default = None):
        """Load JSON from database."""
        try:
            return json.loads(self.get_str(key, default))
        except Exception:
            return default

    def put_json(self, key: str, value):
        """Save JSON to database."""
        self.put_str(key, json.dumps(value))

    def get_source(self, source_id: str, default = None):
        """Load Source object from database."""
        try:
            return Source.from_dict(self.get_json(source_id, default=default))
        except Exception:
            return default

    def put_source(self, source_id: str, source: Source):
        """Save Source object to database."""
        self.put_json(source_id, source.to_dict())

"""Configuration manager for Syndictive."""

from copy import deepcopy
import json
from multiprocessing import Lock
import os

from .. import defaults
from .. import helpers

logger = helpers.create_logger(__name__)


class ConfigManager(dict):
    """This object handles saving and loading of data to a configuration JSON file."""

    def __init__(self, *, filepath: str, default: dict = None):
        super().__init__()
        if not default:
            default = defaults.CONFIG
        self._lock = Lock()
        self._default = default
        self.filepath = filepath

        for key in default:
            self[key] = deepcopy(default[key])

    def _wrap(method):
        def wrapper(self, *args, **kwargs):
            output = method(self, *args, **kwargs)
            self.validate()
            return output
        return wrapper

    __delitem__ = _wrap(dict.__delitem__)
    __setitem__ = _wrap(dict.__setitem__)
    clear = _wrap(dict.clear)
    pop = _wrap(dict.pop)
    popitem = _wrap(dict.popitem)
    setdefault = _wrap(dict.setdefault)
    update = _wrap(dict.update)
    copy = _wrap(dict.copy)

    def deepcopy(self):
        """Return a copy of this dict."""
        return deepcopy(dict(self))

    def validate(self):
        """Config validation."""

        for key, default_value in self._default.items():
            if key not in self:
                self[key] = deepcopy(default_value)
            required_type = type(default_value)
            if not isinstance(self[key], required_type):
                raise TypeError(f"{key} must be of type {required_type}")

    def load(self):
        """Load config from path."""

        if os.path.isfile(self.filepath):
            with open(self.filepath, encoding="utf-8") as file_object:
                raw_config = json.load(file_object)
        else:
            raw_config = {}

        with self._lock:
            self.clear()
            for key, value in raw_config.items():
                self[key] = value

        logger.info(f"Loaded config from {self.filepath}.")

    def save(self):
        """Save config to path."""

        with self._lock:
            self.validate()

            config_to_save = deepcopy(dict(self))
            with open(self.filepath, "w", encoding="utf-8") as file_object:
                json.dump(config_to_save, file_object, indent=4)

        logger.info(f"Saved config to {self.filepath}.")

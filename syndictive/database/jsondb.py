"""Class that handles a so-called database. Actually just a JSON that handles multiprocessing."""

from copy import deepcopy
from dataclasses import dataclass
import json
from multiprocessing import Queue, Manager
import os
from threading import Thread
import time
import traceback
from typing import Any

from ..datastructures import Source
from .. import helpers

logger = helpers.create_logger(__name__)


@dataclass
class WriteTask:
    """Task to tell the manager to write or delete something."""
    key: Any
    value: Any = None
    delete: bool = False


class JsonDBManager:
    """This object handles saving and loading of data to a database."""

    filepaths = []

    def __init__(self, *, filepath: str):
        if filepath in self.filepaths:
            raise Exception(f"{filepath} is already open in a manager!")

        self.filepaths.append(filepath)
        self.filepath = filepath
        self.filepath_temp = f"{filepath}_tmp"
        self._manager = Manager()

        self._database = self._manager.dict()
        if os.path.isfile(filepath):
            with open(self.filepath, "r", encoding="utf-8") as file_object:
                database_contents = json.load(file_object)
            for key, value in database_contents.items():
                self._database[key] = deepcopy(value)
            del database_contents

        self._write_queue = Queue()
        self._write_thread = Thread(target=self._write_worker)
        self._write_thread.start()

        self._save_queue = Queue()
        self._save_thread = Thread(target=self._save_worker)
        self._save_thread.start()

        self._autosave_flag = False
        self._autosave_thread = Thread(target=self._autosave_worker)
        self._autosave_thread.start()

    @property
    def database(self):
        """Read-only pointer to the dict."""
        return self._database

    def _write_worker(self):
        """A worker thread that handles writes to the 'database'."""
        while True:
            task = self._write_queue.get()
            if not isinstance(task, WriteTask):
                continue
            try:
                if task.delete:
                    del self._database[task.key]
                else:
                    self._database[task.key] = task.value
            except Exception:
                logger.error(f"{self.filepath_temp} WRITE ERROR: {traceback.format_exc()}")
            else:
                self._autosave_flag = True

    def _save_worker(self):
        """A worker thread that handles writes to the filesystem."""
        while True:
            self._save_queue.get()
            try:
                logger.info(f"Writing to {self.filepath_temp} ...")
                with open(self.filepath_temp, "w", encoding="utf-8") as file_object:
                    file_object.write(json.dumps(dict(self._database)))
                os.replace(self.filepath_temp, self.filepath)
            except Exception:
                logger.error(f"{self.filepath_temp} SAVE ERROR: {traceback.format_exc()}")

    def _autosave_worker(self):
        """A worker thread that requests periodic automatic writes to the filesystem."""
        while True:
            time.sleep(15)
            if not self._autosave_flag:
                continue
            self._save_queue.put(True)
            self._autosave_flag = False

    def save(self):
        """Manually request a save operation."""
        self._autosave_flag = False
        self._save_queue.put(True)

    def keys(self):
        """Get all keys in the database."""
        for key in self._database.keys():
            yield key

    def get(self, key: Any, default: Any = None):
        """Load some value from database."""
        return self._database.get(key, default)

    def put(self, key: Any, value: Any):
        """Save some value to database."""
        self._write_queue.put(WriteTask(key=key, value=value))

    def delete(self, key: Any):
        """Delete a key from the database."""
        self._write_queue.put(WriteTask(key=key, delete=True))

    def get_json(self, key: Any, default: Any = None):
        """Load JSON from database."""
        try:
            return json.loads(self.get(key, default))
        except Exception:
            return default

    def put_json(self, key: Any, value: Any):
        """Save JSON to database."""
        self.put(key, json.dumps(value))

    def get_source(self, source_id: str, default: Any = None):
        """Load Source object from database."""
        try:
            return Source.from_dict(self.get_json(source_id, default))
        except Exception:
            return default

    def put_source(self, source_id: str, source: Source):
        """Save Source object to database."""
        self.put_json(source_id, source.to_dict())

    def __getitem__(self, key: Any):
        return self._database[key]

    def __setitem__(self, key: Any, value: Any):
        self.put(key, value)

    def __delitem__(self, key: Any):
        self.delete(key)

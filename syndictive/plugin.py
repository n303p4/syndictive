"""Basic plugin core for Syndictive."""

from abc import abstractmethod
from copy import copy
from datetime import datetime
import importlib
import inspect
import json
import os
import re
import sys
import traceback
from urllib.parse import urljoin, quote, quote_plus

from bs4 import BeautifulSoup
from func_timeout import func_timeout
import helium
from logbook import StreamHandler
import requests
import rfeed
try:
    from selenium.webdriver import ChromeOptions
except Exception:
    ChromeOptions = None
from werkzeug.utils import safe_join

from .datastructures import Feed, FeedItem
from . import defaults
from . import helpers

logger = helpers.create_logger(__name__)
if "-v" in sys.argv:
    debug_handler = StreamHandler(sys.stdout, level="DEBUG")
    debug_handler.formatter = defaults.FORMATTER
    debug_handler.push_application()


def get_chromedriver(user_agent: str = None, browser_arguments: list = None):
    """Get a Chrome WebDriver instance. If not available, start one."""
    if not browser_arguments:
        browser_arguments = defaults.BROWSER_ARGUMENTS
    driver = helium.get_driver()
    if not driver:
        logger.debug("Launching browser...")
        options = ChromeOptions()
        for argument in browser_arguments:
            options.add_argument(argument)
        if user_agent:
            options.add_argument(f"user-agent={user_agent}")
        driver = helium.start_chrome(headless=True, options=options)
    return driver


def load_plugins_from_dir(directory: str, blocklist: list[str] = None):
    """Load plugins from a directory."""

    logger.info(f"Loading plugins from directory {directory} ...")
    if not blocklist:
        blocklist = []
    for dirpath, _, filenames in os.walk(directory):
        for filename in filenames:
            if not filename.lower().endswith(".py"):
                continue
            if filename.startswith("__"):
                continue
            fullpath = safe_join(dirpath, filename).split(os.sep)
            module_name = ".".join(fullpath)[:-3]  # Eliminate the .py
            if module_name in blocklist:  # Skip blocklisted modules.
                logger.info(f"{module_name} is blocklisted; skipping...")
                continue
            try:
                module = importlib.import_module(module_name, package=None)
                module_attributes = dir(module)
                plugins_found = 0
                for attribute in module_attributes:
                    item = getattr(module, attribute)
                    if isinstance(item, BasePlugin):
                        plugins_found += 1
                        logger.info(f"Found plugin {attribute} (type {item.__class__.__name__}) in {module_name}")
                if not plugins_found:
                    logger.warning(f"No plugins were found in {module_name}")
            except Exception:  # pylint: disable=broad-except
                logger.error(traceback.format_exc())


def substitute_title(string, title):
    """Substitute title in a string. This is used for search_url and search_selector.

    %t - Title of source
    %q - Title of source, URL quoted with urllib.parse.quote
    %p - Title of source, URL quoted with urllib.parse.quote_plus
    """
    return string.replace("%t", title).replace("%q", quote(title)).replace("%p", quote_plus(title))


class PluginContext:
    """Context class that contains attributes useful for plugins."""

    def __init__(self, *, source_url: str, config: dict, preview_requested: bool = False,
                 title: str = None, rss: bool = False):
        self.source_url = source_url
        self.config = config
        self.preview_requested = preview_requested
        self.title = title
        self.rss = rss


class BasePlugin:
    """Base plugin class for Syndictive.

    Syndictive plugins are *instances* of one of `BasePlugin`'s various subclasses.

    ## Constructor
    * `callable_` - A callable object (either a function or class) to be passed to the plugin for use when running it.
    * `match_urls` - A `list` of regular expressions (`str`). The plugin matches a URL if *any* of the regexes match.
    * `match_selectors` - A `list` of CSS selectors (`str`). The plugin matches a source page if *all* of the selectors
                          match. Selectors must work in the absence of JavaScript execution.
    * `match_none` - A `bool` indicating that the plugin should not match any source. Defaults to `False`; if `True`,
                     then the plugin must be manually selected from the Syndictive interface in order to be used.
    * `notes` - Additional plugin notes, if necessary.
    * `user_agent` - A user agent that the plugin uses when fetching its source.

    ## Other
    * `instances` - A `dict` containing all instances of BasePlugin and its subclasses.
    """

    instances = {}
    most_match_selectors = 0

    def __init__(self, callable_, *, match_urls: list[str] = None, match_selectors: list = None,
                 match_none: bool = False, notes: str = None, user_agent: str = defaults.USER_AGENT):
        name = callable_.__name__
        if name in self.instances:
            raise ValueError(f"A plugin with name {name} is already registered.")
        if not match_none and not match_urls and not match_selectors:
            logger.warning(f"Plugin {name} does not have any of match_none, match_urls, and match_selectors set! "
                           "That means it will be treated as a wildcard and match anything.")
        self.match_none = match_none
        self.match_urls = match_urls or []
        self.match_selectors = match_selectors or []
        if len(self.match_selectors) > BasePlugin.most_match_selectors:
            BasePlugin.most_match_selectors = len(self.match_selectors)
        self.name = name
        if not name.startswith("_"):  # Special case; ignore plugins that start with _
            self.instances[name] = self
        self.notes = notes
        self.user_agent = user_agent

    @classmethod
    def get_for_url(cls, source_url: str):
        """For a given source URL, find a plugin instance that matches. Return `None` if fail."""

        logger.info(f"Determining best plugin to use for {source_url} ...")

        # Sort so that the most complex matching requirements go first
        # Prioritize URL matching over CSS selector matching since it's usually more specific
        plugins = sorted(
            cls.instances.values(),
            key=lambda p: len(p.match_urls) * cls.most_match_selectors * 2 + len(p.match_selectors),
            reverse=True
        )
        html_by_user_agent = {}
        for plugin in plugins:
            if plugin.match_selectors and plugin.user_agent not in html_by_user_agent:
                logger.info(f"Getting HTML of {source_url} w/ user agent {plugin.user_agent} ...")
                try:
                    response = requests.get(source_url, headers={"User-Agent": plugin.user_agent}, timeout=10)
                    if response.status_code < 400:
                        html_by_user_agent[plugin.user_agent] = response.text
                except Exception:
                    logger.error(traceback.format_exc())
            if plugin.matches(source_url, html=html_by_user_agent.get(plugin.user_agent)):
                return plugin
        return None

    def matches(self, source_url: str, *, html: str = None):
        """Check if the plugin instance matches a given source URL."""
        logger.debug(f"Plugin {self.name} is checking for matches against source URL {source_url}")
        if self.match_none:
            logger.debug(f"Plugin {self.name} has match_none set; skipping.")
            return False
        if self.match_urls and not any(re.fullmatch(url, source_url) for url in self.match_urls):
            logger.debug(f"No URLs among {self.match_urls} matched {source_url}")
            return False
        if self.match_selectors:
            if not html:
                try:
                    response = requests.get(source_url, headers={"User-Agent": self.user_agent}, timeout=10)
                    if response.status_code >= 400:
                        return False
                    html = response.text
                except Exception:
                    return False
            soup = BeautifulSoup(html, "html.parser")
            for selector in self.match_selectors:
                logger.debug(f"Checking selector {selector} ...")
                if "||" in selector:
                    selectors = [s.strip() for s in selector.split("||")]
                    if all(not soup.select(s) for s in selectors):
                        logger.debug("Selector did not match!")
                        return False
                else:
                    if not soup.select(selector):
                        logger.debug("Selector did not match!")
                        return False
        logger.debug(f"Plugin {self.name} matches source URL {source_url}")
        return True

    @abstractmethod
    def read_context(self, ctx: PluginContext) -> Feed:
        """Underlying logic to get a feed and preview image out of a PluginContext."""

    def run(self, ctx: PluginContext) -> Feed:
        """Run the plugin instance on a PluginContext."""
        return self.read_context(ctx)


class SimpleGetters:
    """Getters to be subclassed for `SimplePlugin` instances.

    `get_feed()` and `get_preview()` should return an `rfeed.Feed` or `str`, and a `bytes`, respectively.
    """

    def __init__(self, *, plugin: BasePlugin, context: PluginContext):
        self.plugin = plugin
        self.context = context
        self.source_url = context.source_url
        self.config = context.config
        self.preview_requested = context.preview_requested
        self.logger = logger
        self.setup()

    @property
    def ctx(self):
        """Alias for `self.context`."""
        return self.context

    def setup(self):
        """Redefine this method in subclasses to extend/change functionality as desired."""

    @abstractmethod
    def get_feed(self):
        """Return either an `rfeed.Feed` instance, or some raw string data."""
        return rfeed.Feed()

    @abstractmethod
    def get_preview(self):
        """Return a `bytes` representing a preview image."""
        return bytes()


class SimplePlugin(BasePlugin):
    """Simple plugin type, where most of the scraping logic must be implemented in `SimpleGetters` by hand.

    Normally, you should *not* call the `SimplePlugin` constructor directly!
    Instead, use the `simple_plugin()` decorator.
    """

    def __init__(self, callable_, *, match_urls: list[str] = None, match_selectors: list = None,
                 match_none: bool = False, notes: str = None, user_agent: str = defaults.USER_AGENT):
        super().__init__(callable_, match_urls=match_urls, match_selectors=match_selectors,
                         match_none=match_none, notes=notes, user_agent=user_agent)
        if not issubclass(callable_, SimpleGetters):
            raise TypeError(f"For SimplePlugin, callable {self.name} must be subclassed from SimpleGetters")
        callable_parameters = inspect.signature(callable_).parameters.values()
        required_parameters = inspect.signature(SimpleGetters).parameters.values()
        if len(callable_parameters) != len(required_parameters):
            raise ValueError(f"Signature mismatch in {self.name} (length mismatch)")
        for actual, required in zip(callable_parameters, required_parameters):
            if actual.name != required.name:
                raise ValueError(f"Callable signature mismatch in {self.name} (name {actual.name} != {required.name})")
            if actual.kind != required.kind:
                raise ValueError(f"Callable signature mismatch in {self.name} (kind {actual.kind} != {required.kind})")
        self._callable = callable_

    def read_context(self, ctx: PluginContext) -> Feed:
        """Run the plugin instance on a source URL."""
        getters = self._callable(plugin=self, context=ctx)
        feed = Feed()
        feed.contents = getters.get_feed()
        if ctx.preview_requested:
            feed.preview = getters.get_preview()
        return feed


class RSSElementGetters:
    """A collection of getters to be subclassed for instances of `RSSElementsPlugin`.

    `item_limit` is an `int` that controls the maximum number of returned items.
    You can set a global default by setting it on `RSSElementGetters`, or set it per-plugin in `setup()`.
    """

    item_limit = 50

    def __init__(self, *, plugin: BasePlugin, context: PluginContext, api):
        self.plugin = plugin
        self.context = context
        self.source_url = context.source_url
        self.config = context.config
        self.preview_requested = context.preview_requested
        self.api = api
        self.logger = logger
        self.setup()

    @property
    def ctx(self):
        """Alias for `self.context`."""
        return self.context

    @classmethod
    def get_api(cls, ctx):
        """Redefine this method for `LibraryPlugin`. Not used by other plugin types."""

    def setup(self):
        """Redefine this method in subclasses to extend/change functionality as desired."""

    @abstractmethod
    def title(self):
        """Return a `str` representing the RSS feed title."""
        return self.api.title()

    @abstractmethod
    def description(self):
        """Return a `str` representing the RSS feed description."""
        return self.api.description()

    @abstractmethod
    def preview_url(self):
        """Return a `str` representing a preview image URL."""
        return self.api.preview_url()

    @abstractmethod
    def items(self):
        """Return an iterable of items (items can be of any type), to be handled by `self.item_attributes()`."""
        return self.api.items()

    @abstractmethod
    def item_attributes(self, source_item):
        """Parse a single item from `self.items()`.
        Return a `FeedItem` that contains attributes.
        """
        return FeedItem(
            title=source_item.title(),
            link=source_item.link(),
            publication_date=source_item.pubDate()
        )


class RSSElementsPlugin(BasePlugin):
    """Abstract plugin based on `RSSElementGetters`.

    Scraping logic is implemented at a high level, so only simple methods have to be provided.
    """

    def __init__(self, callable_, *, match_urls: list[str] = None, match_selectors: list = None,
                 match_none: bool = False, notes: str = None, method: str = "GET",
                 user_agent: str = defaults.USER_AGENT, search_method: str = "GET", search_url: str = None,
                 search_selector: str = None, browser: bool = False, search_before_redirect: bool = True):
        super().__init__(callable_, match_urls=match_urls, match_selectors=match_selectors,
                         match_none=match_none, notes=notes, user_agent=user_agent)
        if not issubclass(callable_, RSSElementGetters):
            raise TypeError(f"For RSSElementsPlugin, callable {self.name} must be subclassed from RSSElementGetters")
        callable_parameters = inspect.signature(callable_).parameters.values()
        required_parameters = inspect.signature(RSSElementGetters).parameters.values()
        if len(callable_parameters) != len(required_parameters):
            raise ValueError(f"Signature mismatch in {self.name} (length mismatch)")
        for actual, required in zip(callable_parameters, required_parameters):
            if actual.name != required.name:
                raise ValueError(f"Callable signature mismatch in {self.name} (name {actual.name} != {required.name})")
            if actual.kind != required.kind:
                raise ValueError(f"Callable signature mismatch in {self.name} (kind {actual.kind} != {required.kind})")
        if browser and any((search_url, search_selector)):
            logger.warning(
                f"In plugin {self.name}, browser is set to True, which overrides search_url and search_selector."
            )
        if search_url and not search_selector:
            raise ValueError("search_selector is required if search_url is set.")
        if search_selector and not search_url:
            raise ValueError("search_url is required if search_selector is set.")
        self.search_before_redirect = search_before_redirect
        self.browser = browser
        self.method = method
        self.search_method = search_method
        self.search_url = search_url
        self.search_selector = search_selector
        self._callable = callable_

    @abstractmethod
    def get_api(self, response_text: str):
        """To be defined in subclasses."""

    def read_context(self, ctx: PluginContext) -> Feed:
        """Run the plugin instance on a source URL."""

        if not isinstance(ctx.title, str):
            ctx.title = ""

        headers = {"User-Agent": self.user_agent}
        additional_headers = ctx.config.get(f"{self.name}_headers")
        if isinstance(additional_headers, dict):
            logger.debug(f"Adding additional headers {additional_headers} from config/{self.name}_headers...")
            headers |= additional_headers

        if self.browser:  # Mainly useful for sites that are behind Cloudflare
            logger.debug(f"Fetching {ctx.source_url} with browser...")
            driver = get_chromedriver(self.user_agent, defaults.BROWSER_ARGUMENTS)
            logger.debug(f"Navigating to {ctx.source_url}...")
            func_timeout(10, helium.go_to, args=(ctx.source_url,))
            api = self.get_api(driver.page_source)
        else:
            logger.debug(f"Fetching {ctx.source_url} with HTTP {self.method}...")
            method = getattr(requests, self.method.lower())
            response = method(
                ctx.source_url,
                headers=headers,
                timeout=10,
                allow_redirects=not self.search_before_redirect
            )
            api = None
            if response.status_code == 403:
                logger.debug(f"{ctx.source_url} returned HTTP 403 (probably because of Cloudflare).")
                logger.debug(f"Fetching {ctx.source_url} with browser...")
                driver = get_chromedriver(self.user_agent, defaults.BROWSER_ARGUMENTS)
                logger.debug(f"Navigating to {ctx.source_url}...")
                func_timeout(10, helium.go_to, args=(ctx.source_url,))
                api = self.get_api(driver.page_source)
            elif response.status_code >= 300 and self.search_url:
                logger.warning(f"Source returned status code {response.status_code}; trying to get via search...")
                search_method = getattr(requests, self.search_method.lower())
                search_url = substitute_title(self.search_url, ctx.title)
                search_selector = substitute_title(self.search_selector, ctx.title)
                expected_response_url = urljoin(ctx.source_url, search_url)
                search_response = search_method(expected_response_url, headers=headers, timeout=10)
                if search_response.url != expected_response_url:
                    search_response = search_method(
                        urljoin(search_response.url, search_url), headers=headers, timeout=10
                    )
                soup = BeautifulSoup(search_response.text, "html.parser")
                try:
                    new_source_url = soup.select_one(search_selector).get("href")
                except Exception:
                    logger.warning("Falling back on allowing redirect...")
                    new_source_url = method(ctx.source_url, headers=headers, timeout=10).url
                if not new_source_url:
                    raise Exception("Could not find an updated source URL!")
                logger.info(f"Found updated source URL {new_source_url}")
                ctx.source_url = new_source_url
                response = method(
                    new_source_url,
                    headers=headers,
                    timeout=10,
                    allow_redirects=not self.search_before_redirect
                )
            if response.status_code >= 400 and not api:
                raise Exception(f"Source returned status code {response.status_code}")
            if not api:
                api = self.get_api(response.text)

        getters = self._callable(
            plugin=self,
            context=ctx,
            api=api
        )
        feed = self.get_feed(getters)
        if ctx.preview_requested:
            feed.preview = self.get_preview(getters)
        return feed

    def get_preview(self, getters: RSSElementGetters):
        """Get preview image for feed."""
        logger.debug("Getting preview...")
        try:
            preview_url = getters.preview_url()
            if not preview_url:
                return None
            if not preview_url.startswith("http"):
                preview_url = urljoin(getters.source_url, preview_url)
            preview = requests.get(
                preview_url,
                headers={"User-Agent": self.user_agent},
                timeout=10
            ).content
            return preview
        except Exception:
            logger.error(traceback.format_exc())

    def get_feed(self, getters: RSSElementGetters):
        """Generate feed contents."""
        logger.debug("Getting title...")
        try:
            base_title = f"{getters.title()}".replace("\n", " ").strip()
        except Exception:
            logger.error(traceback.format_exc())
            base_title = getters.source_url

        logger.debug("Getting description...")
        try:
            description = f"{getters.description()}".replace("\n", " ").strip()
        except Exception:
            logger.error(traceback.format_exc())
            description = getters.source_url

        source_items = getters.items()
        logger.debug(f"Found {len(source_items)} raw items.")

        logger.debug("Building RSS feed items...")
        feed_items = []
        for source_item in source_items:
            if getters.item_limit > 0 and len(feed_items) >= getters.item_limit:
                logger.debug(f"Stopping build at {getters.item_limit} items.")
                break
            try:
                item_attributes = getters.item_attributes(source_item)
                if not item_attributes:
                    continue

                if isinstance(item_attributes.title, str):
                    title_cleaned = item_attributes.title.replace("\n", " ").strip()
                    if not title_cleaned:
                        logger.debug("Skipping item with blank title")
                        continue
                    if not base_title.lower() in title_cleaned.lower():
                        title_cleaned = f"{title_cleaned} - {base_title}"
                    item_attributes.title = title_cleaned
                if isinstance(item_attributes.link, str) and not item_attributes.link.startswith("http"):
                    item_attributes.link = urljoin(getters.source_url, item_attributes.link)
                if isinstance(item_attributes.image, str) and not item_attributes.image.startswith("http"):
                    item_attributes.image = urljoin(getters.source_url, item_attributes.image)

                logger.debug(f"Building item for {item_attributes.title or 'untitled'}...")

                if not item_attributes.description and not item_attributes.image:
                    item_description = []
                    if base_title:
                        item_description.append(base_title)
                    if description:
                        item_description.append(description)
                    item_attributes.description = "\n".join(item_description) or None

                feed_items.append(item_attributes)
            except Exception:
                logger.error(traceback.format_exc())

        logger.debug("Building RSS feed...")
        feed = Feed(
            title=base_title,
            description=description,
            source_url=getters.source_url,
            language="en-GB",
            last_build_date=datetime.utcnow(),
            items=feed_items
        )

        logger.debug("Done.")

        return feed


class LibraryPlugin(RSSElementsPlugin):
    """A high-level plugin that uses an arbitrary library as its API.

    Normally, you should *not* call the `LibraryPlugin` constructor directly!
    Instead, use the `library_plugin()` decorator.
    """

    def read_context(self, ctx: PluginContext) -> Feed:
        """Run the plugin instance on a source URL."""

        logger.debug(f"Fetching {ctx.source_url}...")
        api = self._callable.get_api(ctx)
        getters = self._callable(
            plugin=self,
            context=ctx,
            api=api
        )
        feed = self.get_feed(getters)
        if ctx.preview_requested:
            feed.preview = self.get_preview(getters)
        return feed


class JSONPlugin(RSSElementsPlugin):
    """A high-level plugin that uses JSON as its API.

    Normally, you should *not* call the `JSONPlugin` constructor directly!
    Instead, use the `json_plugin()` decorator.
    """

    def get_api(self, response_text: str):
        """Return a JSON object from the response text."""
        return json.loads(response_text)


class SoupPlugin(RSSElementsPlugin):
    """A high-level plugin that uses BeautifulSoup4 as its API.

    Normally, you should *not* call the `SoupPlugin` constructor directly!
    Instead, use the `soup_plugin()` decorator.
    """

    def get_api(self, response_text: str):
        """Return a BeautifulSoup object from the response text."""
        return BeautifulSoup(response_text, "html.parser")


class ForwardingGetters:
    """A collection of getters to be subclassed for instances of `ForwardingPlugin`."""

    def __init__(self, *, plugin: BasePlugin, context: PluginContext, api):
        self.plugin = plugin
        self.context = context
        self.source_url = context.source_url
        self.config = context.config
        self.preview_requested = context.preview_requested
        self.api = api
        self.logger = logger
        self.setup()

    @property
    def ctx(self):
        """Alias for `self.context`."""
        return self.context

    def setup(self):
        """Redefine this method in subclasses to extend/change functionality as desired."""

    @abstractmethod
    def feed_url(self):
        """Return a `str` representing the RSS feed URL."""
        return self.api.feed_url()

    @abstractmethod
    def preview_url(self):
        """Return a `str` representing a preview image URL."""
        return self.api.preview_url()


class ForwardingPlugin(SoupPlugin):
    """A variant of SoupPlugin plugin that forwards existing RSS feeds from a site.

    Normally this should be via <link> elements, but some sites are not nice like that.
    """

    def __init__(self, callable_, *, match_urls: list[str] = None, match_selectors: list = None,
                 match_none: bool = False, notes: str = None, method: str = "GET",
                 user_agent: str = defaults.USER_AGENT):
        BasePlugin.__init__(self, callable_, match_urls=match_urls, match_selectors=match_selectors,
                            match_none=match_none, notes=notes, user_agent=user_agent)
        if not issubclass(callable_, ForwardingGetters):
            raise TypeError(f"For ForwardingPlugin, callable {self.name} must be subclassed from ForwardingGetters")
        callable_parameters = inspect.signature(callable_).parameters.values()
        required_parameters = inspect.signature(ForwardingGetters).parameters.values()
        if len(callable_parameters) != len(required_parameters):
            raise ValueError(f"Signature mismatch in {self.name} (length mismatch)")
        for actual, required in zip(callable_parameters, required_parameters):
            if actual.name != required.name:
                raise ValueError(f"Callable signature mismatch in {self.name} (name {actual.name} != {required.name})")
            if actual.kind != required.kind:
                raise ValueError(f"Callable signature mismatch in {self.name} (kind {actual.kind} != {required.kind})")
        self.method = method
        self._callable = callable_

    def get_preview_(self, getters: ForwardingGetters, feed_soup: BeautifulSoup):
        """Get preview image for feed."""
        logger.debug("Getting preview...")
        try:
            preview_url_element = feed_soup.find("url") or feed_soup.find("logo") or feed_soup.find("icon")
            if not preview_url_element:
                return None
            preview_url = preview_url_element.text
            if not preview_url.startswith("http"):
                preview_url = urljoin(getters.source_url, preview_url)
            logger.info(preview_url)
            preview = requests.get(
                preview_url,
                headers={"User-Agent": self.user_agent},
                timeout=10
            ).content
            return preview
        except Exception:
            logger.error(traceback.format_exc())

    def get_feed(self, getters: ForwardingGetters):
        logger.debug("Getting feed...")
        try:
            feed_url = getters.feed_url()
            if not feed_url:
                return Feed(), None
            if not feed_url.startswith("http"):
                feed_url = urljoin(getters.source_url, feed_url)
            feed_contents = requests.get(
                feed_url,
                headers={"User-Agent": self.user_agent},
                timeout=10
            ).text
            feed = Feed(contents=feed_contents)
            try:
                feed_soup = BeautifulSoup(feed_contents, "html.parser")
                if getters.context.rss:
                    for element in ("title", "description"):
                        try:
                            setattr(feed, element, feed_soup.find(element).text)
                        except Exception:
                            pass
                else:
                    try:
                        feed.title = feed_soup.find("title").text
                    except Exception:
                        pass
                    try:
                        feed.description = feed_soup.find("subtitle").text
                    except Exception:
                        pass
            except Exception:
                pass
            return feed, feed_soup
        except Exception:
            logger.error(traceback.format_exc())
        return Feed(), None

    def read_context(self, ctx: PluginContext) -> Feed:
        """Run the plugin instance on a source URL."""

        headers = {"User-Agent": self.user_agent}
        additional_headers = ctx.config.get(f"{self.name}_headers")
        if isinstance(additional_headers, dict):
            logger.debug(f"Adding additional headers {additional_headers} from config/{self.name}_headers...")
            headers |= additional_headers

        logger.debug(f"Fetching {ctx.source_url} with HTTP {self.method}...")
        method = getattr(requests, self.method.lower())
        response = method(ctx.source_url, headers=headers, timeout=10)
        if response.status_code >= 400:
            raise Exception(f"Source returned status code {response.status_code}")
        soup = BeautifulSoup(response.text, "html.parser")

        getters = self._callable(
            plugin=self,
            context=ctx,
            api=soup
        )
        feed, feed_soup = self.get_feed(getters)
        if ctx.preview_requested and feed_soup:
            feed.preview = self.get_preview_(getters, feed_soup)
        return feed


class BrowserPlugin(RSSElementsPlugin):
    """A high-level plugin that uses a Helium+Selenium ChromeDriver instance as its API.

    Normally, you should *not* call the `BrowserPlugin` constructor directly!
    Instead, use the `browser_plugin()` decorator.
    """

    def __init__(self, callable_, *, match_urls: list[str] = None, match_selectors: list = None,
                 match_none: bool = False, notes: str = None, method: str = "GET",
                 user_agent: str = defaults.USER_AGENT, browser_arguments: list[str] = None):
        super().__init__(callable_, match_urls=match_urls, match_selectors=match_selectors,
                         match_none=match_none, notes=notes, method=method, user_agent=user_agent)
        if not ChromeOptions:
            raise ImportError("Cannot initialize BrowserPlugin! Selenium is not installed.")
        if not browser_arguments:
            browser_arguments = defaults.BROWSER_ARGUMENTS
        if not (isinstance(browser_arguments, list) and all(isinstance(arg, str) for arg in browser_arguments)):
            raise ValueError("browser_arguments must be a list of str")
        self.browser_arguments = copy(browser_arguments)

    def read_context(self, ctx: PluginContext) -> Feed:
        """Run the plugin instance on a source URL."""

        driver = get_chromedriver(self.user_agent, self.browser_arguments)
        logger.debug(f"Navigating to {ctx.source_url}...")
        func_timeout(10, helium.go_to, args=(ctx.source_url,))

        getters = self._callable(
            plugin=self,
            context=ctx,
            api=driver
        )
        feed = self.get_feed(getters)
        if ctx.preview_requested:
            feed.preview = self.get_preview(getters)
        return feed


def simple_plugin(match_urls: list[str] = None, match_selectors: list = None,
                  match_none: bool = False, notes: str = None, user_agent: str = defaults.USER_AGENT):
    """Decorator to convert a subclass of `SimpleGetters` to a `SimplePlugin` instance.

    * `match_urls` - A `list` of regular expressions (`str`). The plugin matches a URL if *any* of the regexes match.
    * `match_selectors` - A `list` of CSS selectors (`str`). The plugin matches a source page if *all* of the selectors
                          match. Selectors must work in the absence of JavaScript execution.
    * `match_none` - A `bool` indicating that the plugin should not match any source. Defaults to `False`; if `True`,
                     then the plugin must be manually selected from the Syndictive interface in order to be used.
    * `notes` - Additional plugin notes, if necessary.
    * `user_agent` - A user agent (`str`) that the plugin uses in HTTP requests when fetching its source.
    """
    def decorator(callable_):
        plugin = SimplePlugin(callable_, match_urls=match_urls, match_selectors=match_selectors,
                              match_none=match_none, notes=notes, user_agent=user_agent)
        return plugin
    return decorator


def library_plugin(match_urls: list[str] = None, match_selectors: list = None,
                match_none: bool = False, notes: str = None, user_agent: str = defaults.USER_AGENT):
    """Decorator to convert a subclass of `RSSElementGetters` to a `LibraryPlugin` instance.

    * `match_urls` - A `list` of regular expressions (`str`). The plugin matches a URL if *any* of the regexes match.
    * `match_selectors` - A `list` of CSS selectors (`str`). The plugin matches a source page if *all* of the selectors
                          match. Selectors must work in the absence of JavaScript execution.
    * `match_none` - A `bool` indicating that the plugin should not match any source. Defaults to `False`; if `True`,
                     then the plugin must be manually selected from the Syndictive interface in order to be used.
    * `notes` - Additional plugin notes, if necessary.
    * `user_agent` - A user agent (`str`) that the plugin uses in HTTP requests when fetching its source.
    """
    def decorator(callable_: RSSElementGetters):
        plugin = LibraryPlugin(callable_, match_urls=match_urls, match_selectors=match_selectors,
                               match_none=match_none, notes=notes, user_agent=user_agent)
        return plugin
    return decorator


def json_plugin(match_urls: list[str] = None, match_selectors: list = None,
                match_none: bool = False, notes: str = None, method: str = "GET",
                user_agent: str = defaults.USER_AGENT, search_url: str = None,
                search_selector: str = None, browser: bool = False):
    """Decorator to convert a subclass of `RSSElementGetters` to a `JSONPlugin` instance.

    * `match_urls` - A `list` of regular expressions (`str`). The plugin matches a URL if *any* of the regexes match.
    * `match_selectors` - A `list` of CSS selectors (`str`). The plugin matches a source page if *all* of the selectors
                          match. Selectors must work in the absence of JavaScript execution.
    * `match_none` - A `bool` indicating that the plugin should not match any source. Defaults to `False`; if `True`,
                     then the plugin must be manually selected from the Syndictive interface in order to be used.
    * `notes` - Additional plugin notes, if necessary.
    * `method` - HTTP method to use for the given source.
    * `user_agent` - A user agent (`str`) that the plugin uses in HTTP requests when fetching its source.
    """
    def decorator(callable_: RSSElementGetters):
        plugin = JSONPlugin(callable_, match_urls=match_urls, match_selectors=match_selectors,
                            match_none=match_none, notes=notes, method=method, user_agent=user_agent,
                            search_url=search_url, search_selector=search_selector, browser=browser)
        return plugin
    return decorator


def forwarding_plugin(match_urls: list[str] = None, match_selectors: list = None,
                      match_none: bool = False, notes: str = None, method: str = "GET",
                      user_agent: str = defaults.USER_AGENT):
    """Decorator to convert a subclass of `ForwardingGetters` to a `ForwardingPlugin` instance.

    * `match_urls` - A `list` of regular expressions (`str`). The plugin matches a URL if *any* of the regexes match.
    * `match_selectors` - A `list` of CSS selectors (`str`). The plugin matches a source page if *all* of the
                          selectors match. Selectors must work in the absence of JavaScript execution.
    * `match_none` - A `bool` indicating that the plugin should not match any source. Defaults to `False`; if `True`,
                     then the plugin must be manually selected from the Syndictive interface in order to be used.
    * `notes` - Additional plugin notes, if necessary.
    * `method` - HTTP method to use for the given source.
    * `user_agent` - A user agent (`str`) that the plugin uses in HTTP requests when fetching its source.
    """
    def decorator(callable_: ForwardingGetters):
        plugin = ForwardingPlugin(callable_, match_urls=match_urls, match_selectors=match_selectors,
                                  match_none=match_none, notes=notes, method=method, user_agent=user_agent)
        return plugin
    return decorator


def soup_plugin(match_urls: list[str] = None, match_selectors: list = None,
                match_none: bool = False, notes: str = None, method: str = "GET",
                user_agent: str = defaults.USER_AGENT, search_url: str = None,
                search_selector: str = None, browser: bool = False, search_before_redirect: bool = True):
    """Decorator to convert a subclass of `RSSElementGetters` to a `SoupPlugin` instance.

    * `match_urls` - A `list` of regular expressions (`str`). The plugin matches a URL if *any* of the regexes match.
    * `match_selectors` - A `list` of CSS selectors (`str`). The plugin matches a source page if *all* of the
                          selectors match. Selectors must work in the absence of JavaScript execution.
    * `match_none` - A `bool` indicating that the plugin should not match any source. Defaults to `False`; if `True`,
                     then the plugin must be manually selected from the Syndictive interface in order to be used.
    * `notes` - Additional plugin notes, if necessary.
    * `method` - HTTP method to use for the given source.
    * `user_agent` - A user agent (`str`) that the plugin uses in HTTP requests when fetching its source.
    """
    def decorator(callable_: RSSElementGetters):
        plugin = SoupPlugin(callable_, match_urls=match_urls, match_selectors=match_selectors,
                            match_none=match_none, notes=notes, method=method, user_agent=user_agent,
                            search_url=search_url, search_selector=search_selector, browser=browser,
                            search_before_redirect=search_before_redirect)
        return plugin
    return decorator


def browser_plugin(match_urls: list[str] = None, match_selectors: list = None,
                   match_none: bool = False, notes: str = None, user_agent: str = defaults.USER_AGENT,
                   browser_arguments: list[str] = None):
    """Decorator to convert subclass of `RSSElementGetters` to a `BrowserPlugin` instance.

    * `match_urls` - A `list` of regular expressions (`str`). The plugin matches a URL if *any* of the regexes match.
    * `match_selectors` - A `list` of CSS selectors (`str`). The plugin matches a source page if *all* of the
                          selectors match. Selectors must work in the absence of JavaScript execution.
    * `match_none` - A `bool` indicating that the plugin should not match any source. Defaults to `False`; if `True`,
                     then the plugin must be manually selected from the Syndictive interface in order to be used.
    * `notes` - Additional plugin notes, if necessary.
    * `method` - HTTP method to use for the given source.
    * `user_agent` - A user agent (`str`) that the plugin uses in HTTP requests when fetching its source.
    * `browser_arguments` - A list of additional arguments to provide to the ChromeDriver instance.
    """
    def decorator(callable_: RSSElementGetters):
        plugin = BrowserPlugin(callable_, match_urls=match_urls, match_selectors=match_selectors,
                               match_none=match_none, notes=notes, user_agent=user_agent,
                               browser_arguments=browser_arguments)
        return plugin
    return decorator


def get_plugin_for_source(source_url: str):
    """Given a source URL, get the first plugin that matches it."""
    return BasePlugin.get_for_url(source_url)

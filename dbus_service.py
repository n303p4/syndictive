"""Bridge D-Bus service

Some Linux distros do not have dbus-python packaged for Python 3.10+, but Syndictive depends on it.
This service exists as a workaround for that situation, and can be run in Python versions lower than 3.10.
Simply run this in your distro's usual python3 interpreter.
"""

import json

import dbus
from flask import Flask, request, abort

app = Flask(__name__)


def akregator_dbus_interface():
    return dbus.SessionBus().get_object("org.kde.akregator", "/Akregator")


@app.route("/fetchAllFeeds", methods=["GET", "POST"])
def fetch_all_feeds():
    akregator_dbus_interface().fetchAllFeeds()
    return "OK", 200


@app.route("/exportFile", methods=["POST"])
def export_file():
    try:
        request_body = request.get_json()
        url = request_body["url"]
        akregator_dbus_interface().exportFile(url)
    except Exception:
        abort(400)
    return "OK", 200


@app.route("/addFeedsToGroup", methods=["POST"])
def add_feeds_to_group():   
    try:
        request_body = request.get_json()
        lst = request_body["lst"]
        feedname = request_body["feedname"]
        akregator_dbus_interface().addFeedsToGroup(lst, feedname)
    except Exception:
        abort(400)
    return "OK", 200


def main(config_filepath: str = "config.json"):
    """Main driver."""

    with open(config_filepath, encoding="utf-8") as file_object:
        config = json.load(file_object)
    app.run(port=config["port_number_dbus"])


if __name__ == "__main__":
    main()

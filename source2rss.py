"""Command line Syndictive tool."""

import argparse
from os import path
import sys

from helium import kill_browser

import syndictive

syndictive.plugin.logger.disable()


def main():
    """Read in one URL and convert it to an RSS feed, if possible."""

    syndictive.load_plugins_from_dir("plugins")

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("-s", "--source-url", metavar="SOURCE_URL", type=str, required=True,
                        help="URL of source page.")
    parser.add_argument("-o", "--output-filepath", metavar="FILEPATH", type=str, default=None,
                        help="Optional path to save file to.")
    parser.add_argument("-p", "--plugin", metavar="PLUGIN", type=str, default=None,
                        help=f"Specify a plugin manually. Valid plugins are: {', '.join(syndictive.plugins)}")
    args = parser.parse_args()

    if args.plugin:
        plugin = syndictive.plugins.get(args.plugin)
    else:
        plugin = syndictive.get_plugin_for_source(args.source_url)
    if not plugin:
        if args.plugin:
            print(f"Invalid plugin name! Valid plugins are: {', '.join(syndictive.plugins)}")
        else:
            print("Unrecognized URL.")
        sys.exit(1)
    context = syndictive.PluginContext(
        source_url=args.source_url,
        config={},
        preview_requested=False
    )
    feed = plugin.run(context)
    try:
        kill_browser()
    except Exception:
        pass

    if args.output_filepath:
        with open(path.expanduser(args.output_filepath), "w", encoding="utf-8") as file_object:
            file_object.write(feed.to_rss())
    else:
        print(feed.to_rss())


if __name__ == "__main__":
    main()

#!/usr/bin/env python3
# pylint: disable=C0103

"""
Generate documentation for syndictive.plugin

Depends on pdoc3, not upstream pdoc.
"""

# https://stackoverflow.com/questions/33517072/

import pdoc

module = pdoc.import_module("syndictive.plugin")
documentation = pdoc.Module(module)
print(documentation.html())

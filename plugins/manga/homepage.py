"""Komiko HomePage plugin (e.g. LynxScans)"""

import syndictive


@syndictive.soup_plugin(match_selectors=["a[href*='/volume/'][href*='/chapter/']"])
class homepage(syndictive.RSSElementGetters):
    """Load a Komiko HomePage comic page and convert it to RSS."""

    item_limit = 100

    def setup(self):
        self.metadata = {}
        for meta in self.api.find_all("meta"):
            name = meta.get("property") or meta.get("name")
            content = meta.get("content")
            if name and content:
                self.metadata[name] = content

        mark = False
        self.last_updated = None
        for paragraph in self.api.find_all("p"):
            if "last updated" in str(paragraph.text).lower():
                mark = True
            elif mark:
                self.last_updated = syndictive.to_datetime(paragraph.text)
                break

    def title(self):
        for prefix in "og:", "twitter:":
            content = self.metadata.get(f"{prefix}title")
            if content:
                return content
        return self.api.find("title").text

    def description(self):
        for prefix in "", "og:", "twitter:":
            content = self.metadata.get(f"{prefix}description")
            if content:
                return content
        return self.source_url

    def preview_url(self):
        for prefix in "og:", "twitter:":
            content = self.metadata.get(f"{prefix}image")
            if content:
                return content
        return self.api.find("img").get("src")

    def items(self):
        return list(self.api.select("a[href*='/volume/'][href*='/chapter/']"))

    def item_attributes(self, source_item):
        return syndictive.FeedItem(
            title=source_item.text,
            link=source_item.get("href"),
            publication_date=self.last_updated
        )

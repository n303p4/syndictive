"""Ten Manga plugin."""

from datetime import datetime

import syndictive


@syndictive.soup_plugin(
    match_selectors=["div.bk-name", "div.bk-info-summary",
                     "div.bk-cover > a > img", "div.chp-ls"],
    notes="Chapters on Ten Manga are hosted on https://www.gardenmanage.com "
          "and sometimes randomly don't load. It is recommended to use another "
          "manga site if possible."
)
class tenmanga(syndictive.RSSElementGetters):
    """Load a Ten Manga page and convert it to RSS."""

    def setup(self):
        author_string = []
        tags = self.api.select(".bk-attr-item-child")
        for tag in tags:
            tag_text = tag.text.lower()
            if not "author" in tag_text:
                continue
            author_tags = tag.select("a")
            for author_tag in author_tags:
                try:
                    author = author_tag.text.strip()
                except Exception:
                    continue
                if author not in author_string:
                    author_string.append(author)
        self.author = ", ".join(author_string) or None

    def title(self):
        return self.api.select_one("div.bk-name").text

    def description(self):
        return self.api.select_one("div.bk-info-summary").text

    def preview_url(self):
        return self.api.select_one("div.bk-cover > a > img").get("lazy_url")

    def items(self):
        return self.api.select_one("div.chp-ls").select("div.chp-item")

    def item_attributes(self, source_item):
        anchor = source_item.select_one("a")
        timestring = anchor.select_one("td.chp-time").text
        if "ago" in timestring:
            item_pubDate = syndictive.to_datetime(timestring)
        else:
            item_pubDate = datetime.strptime(timestring, "%Y-%m-%d")
        return syndictive.FeedItem(
            title=anchor.select_one("td.chp-idx").text,
            link=anchor.get("href"),
            publication_date=item_pubDate,
            author=self.author
        )

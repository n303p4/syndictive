"""tapas.io plugin."""

import syndictive


@syndictive.soup_plugin(
    match_urls=[r"https://tapas.io/series/.*/info"]
)
class tapasio(syndictive.RSSElementGetters):
    """Load a MangaStream page and convert it to RSS."""

    def title(self):
        return self.api.select_one("a.title").text

    def description(self):
        return self.api.select_one("p.description").text

    def preview_url(self):
        return self.api.select_one("div.thumb-wrapper > a.thumb > img").get("src")

    def items(self):
        return self.api.select_one("ul.episode-list").find_all("li")

    def item_attributes(self, source_item):
        anchor = source_item.select_one("a")
        try:
            item_title = anchor.select_one("p.title").text
        except Exception:
            item_title = None
        try:
            timestring = anchor.select_one("p.additional > span").text
        except Exception:
            timestring = anchor.select_one("p.additional").text
        item_pubDate = syndictive.to_datetime(timestring)
        return syndictive.FeedItem(
            title=item_title,
            link=anchor.get("href"),
            publication_date=item_pubDate
        )

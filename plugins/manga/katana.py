"""MangaKatana plugin."""

from datetime import datetime

import syndictive


@syndictive.soup_plugin(
    match_selectors=["div.summary", "div.chapters", "div.cover > img"]
)
class katana(syndictive.RSSElementGetters):
    """Load a MangaKatana page and convert it to RSS."""

    def setup(self):
        try:
            self.author = self.api.select_one(".author").text
        except Exception:
            self.author = None

    def title(self):
        return self.api.find("title").text

    def description(self):
        return self.api.select_one("div.summary").text

    def preview_url(self):
        return self.api.select_one("div.cover > img").get("src")

    def items(self):
        return self.api.select_one("div.chapters").find_all("tr")

    def item_attributes(self, source_item):
        anchor = source_item.select_one("div.chapter > a")
        return syndictive.FeedItem(
            title=anchor.text,
            link=anchor.get("href"),
            publication_date=datetime.strptime(source_item.select_one("div.update_time").text, "%b-%d-%Y"),
            author=self.author
        )

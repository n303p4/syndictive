"""Madara plugin."""

from datetime import datetime
from urllib.parse import urlparse

from bs4 import BeautifulSoup
import requests

import syndictive


@syndictive.soup_plugin(
    match_selectors=["div.description-summary || div.manga-excerpt", "div.summary_image > a > img"]
)
class madara(syndictive.RSSElementGetters):
    """Load a Madara Wordpress page and convert it to RSS."""

    def setup(self):
        self.chapter_url = f"{self.source_url}/ajax/chapters"
        self.headers = {
            "User-Agent": self.plugin.user_agent
        }
        hostname = urlparse(self.source_url).hostname
        date_formats = self.config.get("madara_date_formats", {})
        self.date_format = date_formats.get(hostname)
        author_string = []
        for selector in (".author-content", ".artist-content"):
            try:
                author = self.api.select_one(selector).text.strip()
            except Exception:
                continue
            if author not in author_string:
                author_string.append(author)
        self.author_string = ", ".join(author_string) or None

    def title(self):
        return self.api.find("h1").text

    def description(self):
        for description_selector in ("div.description-summary", "div.manga-excerpt"):
            element = self.api.select_one(description_selector)
            if element:
                return element.text
        return ""

    def preview_url(self):
        preview_image = self.api.select_one("div.summary_image > a > img")
        if not preview_image:
            return None
        return preview_image.get("data-src", preview_image.get("src"))

    def items(self):
        items = self.api.select("ul.version-chap > li")
        if not items:
            item_response = requests.post(self.chapter_url, headers=self.headers)
            item_soup = BeautifulSoup(item_response.text, "html.parser")
            items = item_soup.select("ul.version-chap > li")
        return items

    def item_attributes(self, source_item):
        anchor = source_item.find("a")

        item_title = anchor.text
        item_link = anchor.get("href")

        time = source_item.select_one("span.chapter-release-date")
        if time:
            time_anchor = time.find("a")
            if time_anchor:
                timestring = time_anchor.get("title")
            else:
                timestring = time.text.strip()
            if self.date_format and not "ago" in timestring:
                item_pubDate = datetime.strptime(timestring, self.date_format)
            else:
                item_pubDate = syndictive.to_datetime(timestring)
        else:
            item_pubDate = datetime(year=1970, month=1, day=1)
        return syndictive.FeedItem(
            title=item_title,
            link=item_link,
            publication_date=item_pubDate,
            author=self.author_string
        )

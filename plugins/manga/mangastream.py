"""MangaStream plugin."""

import syndictive


@syndictive.soup_plugin(
    match_selectors=["h1.entry-title", "div.wd-full", "div#chapterlist", "div.thumb > img"],
    search_url=r"/?s=%p",
    search_selector='a[title="%t"]',
    search_before_redirect=True
)
class mangastream(syndictive.RSSElementGetters):
    """Load a MangaStream page and convert it to RSS."""

    def setup(self):
        author_string = []
        tags = self.api.select(".infox .fmed")
        for tag in tags:
            tag_text = tag.text.lower()
            if not ("author" in tag_text or "artist" in tag_text):
                continue
            try:
                author = tag.select_one("span").text.strip()
            except Exception:
                continue
            if author not in author_string:
                author_string.append(author)
        self.author = ", ".join(author_string) or None

    def title(self):
        return self.api.select_one("h1.entry-title").text

    def description(self):
        return self.api.select_one("div[itemprop=description]").text

    def preview_url(self):
        return self.api.select_one("div.thumb > img").get("src")

    def items(self):
        return self.api.select_one("div#chapterlist").find_all("li")

    def item_attributes(self, source_item):
        anchor = source_item.select_one("a")
        item_title = anchor.select_one("span.chapternum").text
        timestring = anchor.select_one("span.chapterdate").text
        item_pubDate = syndictive.to_datetime(timestring)
        return syndictive.FeedItem(
            title=item_title,
            link=anchor.get("href"),
            publication_date=item_pubDate,
            author=self.author
        )

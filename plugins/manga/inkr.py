"""Various manga plugins for Syndictive."""

import json

import syndictive


@syndictive.soup_plugin(match_urls=[r"https://comics.inkr.com/title/.*/chapters"])
class inkr(syndictive.RSSElementGetters):
    """comics.inkr.com plugin"""

    item_limit = 20

    def setup(self):
        self.item_list = {}
        item_list_candidates = self.api.select("script[type='application/ld+json']")
        for candidate in item_list_candidates:
            if '"ItemList"' in candidate.text:
                self.item_list = json.loads(candidate.text)
                break

    def title(self):
        return self.api.find("h1").text

    def description(self):
        return self.api.select_one("meta[name='description']").get("content")

    def preview_url(self):
        return self.api.select_one("link[as='image']").get("href")

    def items(self):
        return self.item_list.get("itemListElement", [])[::-1]

    def item_attributes(self, source_item):
        return syndictive.FeedItem(
            title=f"Chapter {source_item.get('position', '#####')}",
            link=source_item.get("url")
        )

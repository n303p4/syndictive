"""Genkan.io plugin."""

import syndictive


@syndictive.soup_plugin(
    match_selectors=[".text-muted", ".navbar", ".nav-item"]
)
class genkanio(syndictive.RSSElementGetters):
    """Load a Genkan.io page and convert it to RSS."""

    def setup(self):
        self.metadata = {}
        for meta in self.api.find_all("meta"):
            name = meta.get("property") or meta.get("name")
            content = meta.get("content")
            if name and content:
                self.metadata[name] = content

    def title(self):
        return self.metadata.get("og:title")

    def description(self):
        return self.metadata.get("og:description")

    def preview_url(self):
        return self.metadata.get("og:image")

    def items(self):
        raw_anchors = self.api.find_all("a")
        merged_anchors = {}
        for anchor in raw_anchors:
            anchor_href = anchor.get("href")
            if not anchor_href.startswith(self.source_url):
                continue
            if anchor_href not in merged_anchors:
                merged_anchors[anchor_href] = syndictive.FeedItem(link=anchor_href)
            if "chapter" in anchor.text.lower():
                merged_anchors[anchor_href].title = anchor.text.strip()
            else:
                merged_anchors[anchor_href].publication_date = syndictive.to_datetime(anchor.text)
        return merged_anchors.values()

    def item_attributes(self, source_item):
        return source_item

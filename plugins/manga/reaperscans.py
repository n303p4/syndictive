"""Reaper Scans plugin."""

import syndictive


@syndictive.soup_plugin(
    match_urls=[r"https://reaperscans.com/comics/.*", r"https://reaperscans.com/novels/.*"],
    browser=True
)
class reaperscans(syndictive.RSSElementGetters):
    """Load a Reaper Scans page and convert it to RSS."""

    def title(self):
        return self.api.find("h1").text

    def description(self):
        return self.api.select_one('p.prose').text

    def preview_url(self):
        return self.api.select_one('img[src*="https://media.reaperscans.com"]').get("src")

    def items(self):
        raw_items = self.api.find_all("li")
        filtered_items = []
        for item in raw_items:
            anchor = item.find("a")
            if self.source_url in anchor.get("href"):
                filtered_items.append(item)
        return filtered_items

    def item_attributes(self, source_item):
        anchor = source_item.select_one("a")
        item_title = anchor.select_one("div.text-sm > p").text
        timestring = anchor.select_one("div.mt-2").text
        item_pubDate = syndictive.to_datetime(timestring)
        return syndictive.FeedItem(
            title=item_title,
            link=anchor.get("href"),
            publication_date=item_pubDate
        )

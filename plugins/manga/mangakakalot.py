"""MangaKakalot plugin."""

from datetime import datetime

import syndictive


@syndictive.soup_plugin(
    match_selectors=["ul.manga-info-text > li > h1", "div#noidungm",
                     "div.manga-info-pic > img", "div.chapter-list"]
)
class mangakakalot(syndictive.RSSElementGetters):
    """Load a MangaKakalot page and convert it to RSS."""

    def setup(self):
        author_string = []
        tags = self.api.select("ul.manga-info-text > li")
        for tag in tags:
            tag_text = tag.text.lower()
            if not "author" in tag_text:
                continue
            author_tags = tag.select("a")
            for author_tag in author_tags:
                try:
                    author = author_tag.text.strip()
                except Exception:
                    continue
                if author not in author_string:
                    author_string.append(author)
        self.author = ", ".join(author_string) or None

    def title(self):
        return self.api.select_one("ul.manga-info-text > li > h1").text

    def description(self):
        return self.api.select_one("div#noidungm").text

    def preview_url(self):
        return self.api.select_one("div.manga-info-pic > img").get("src")

    def items(self):
        return self.api.select_one("div.chapter-list").select("div.row")

    def item_attributes(self, source_item):
        anchor = source_item.find("a")
        time = source_item.select_one("span[title]")
        timestring = time.get("title").strip()
        if "ago" in timestring:
            item_pubDate = syndictive.to_datetime(timestring)
        else:
            item_pubDate = datetime.strptime(timestring, "%b-%d-%Y %H:%M")
        return syndictive.FeedItem(
            title=anchor.text,
            link=anchor.get("href"),
            publication_date=item_pubDate,
            author=self.author
        )

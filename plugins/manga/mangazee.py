"""MangaZee plugin."""

from datetime import datetime

import syndictive


@syndictive.soup_plugin(
    match_urls=[r"https://www.mangazee.com/manga/.*"],
    match_selectors=["h1.entry-title", "div[itemprop=description]",
                     "img[itemprop=image]", "div#chapterlist"],
    notes="The MangaZee site frequently has image loading problems."
)
class mangazee(syndictive.RSSElementGetters):
    """Load a MangaZee page and convert it to RSS."""

    def title(self):
        return self.api.select_one("h1.entry-title").text

    def description(self):
        return self.api.select_one("div[itemprop=description]").text

    def preview_url(self):
        return self.api.select_one("img[itemprop=image]").get("src")

    def items(self):
        return self.api.select_one("div#chapterlist").find_all("li")

    def item_attributes(self, source_item):
        anchor = source_item.find("a")
        return syndictive.FeedItem(
            title=anchor.select_one("span.chapternum").text,
            link=anchor.get("href"),
            publication_date=datetime.strptime(anchor.select_one("span.chapterdate").text, "%B %d, %Y")
        )

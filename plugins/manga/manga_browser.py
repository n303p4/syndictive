"""Various stupider manga plugins for Syndictive."""

from datetime import datetime

from dateutil.parser import parse as parse_timestr
import helium
import parsedatetime
from selenium.webdriver.common.by import By

import syndictive


@syndictive.browser_plugin(
    match_urls=[r"https://manhuaplus.com/manga/.*"],
    match_selectors=["div.description-summary", "div.summary_image > a > img"],
    notes="When possible, you should prefer the regular madara plugin."
)
class madara_browser(syndictive.RSSElementGetters):
    """Load a Madara Wordpress page with browser requirements, and convert it to RSS."""

    def setup(self):
        self.calendar = parsedatetime.Calendar()

    def title(self):
        return helium.S("div.post-title > h1").web_element.get_attribute("innerText")

    def description(self):
        return helium.S("div.summary__content").web_element.get_attribute("innerText")

    def preview_url(self):
        return helium.S('div.summary_image > a > img').web_element.get_attribute("src")

    def items(self):
        chapter_anchor_predicate = helium.S('ul.version-chap > li > a')
        helium.wait_until(chapter_anchor_predicate.exists)
        return helium.find_all(chapter_anchor_predicate)

    def item_attributes(self, source_item):
        anchor = source_item.web_element
        item_title = anchor.get_attribute("innerText")
        item_link = anchor.get_attribute("href")

        parent_of_parent = anchor.find_element(By.XPATH, "./../..")
        time = parent_of_parent.find_element(By.TAG_NAME, "span")
        timestring = time.get_attribute("innerText")
        item_pubDate = None
        if "ago" in timestring:
            try:
                item_pubDate, _ = self.calendar.parseDT(timestring)
            except Exception:
                pass
        else:
            try:
                item_pubDate = parse_timestr(timestring)
            except Exception:
                pass
        return syndictive.FeedItem(
            title=item_title,
            link=item_link,
            publication_date=item_pubDate
        )


@syndictive.browser_plugin(
    match_none=True,
    notes="When possible, you should prefer the regular mangadex plugin."
)
class mangadex_browser(syndictive.RSSElementGetters):
    """Load a Mangadex page and convert it to RSS."""

    def setup(self):
        self.languages = ["English"]

    def title(self):
        return helium.S("p.mb-1").web_element.get_attribute("innerText")

    def description(self):
        return helium.S(".readmore").web_element.get_attribute("innerText")

    def preview_url(self):
        return helium.S('img[src*="https://mangadex.org/covers"]').web_element.get_attribute("src")

    def items(self):
        chapter_anchor_predicates = [helium.S(f'img[title="{lang}"] + a') for lang in self.languages]
        items = []
        for predicate in chapter_anchor_predicates:
            helium.wait_until(predicate.exists)
            items += list(helium.find_all(predicate))
        return items

    def item_attributes(self, source_item):
        anchor = source_item.web_element
        item_title = anchor.get_attribute("innerText")
        item_link = anchor.get_attribute("href")

        parent_of_parent = anchor.find_element(By.XPATH, "./../..")
        time = parent_of_parent.find_element(By.TAG_NAME, "time")
        item_pubDate = datetime.fromisoformat(time.get_attribute("datetime"))

        return syndictive.FeedItem(
            title=item_title,
            link=item_link,
            publication_date=item_pubDate
        )

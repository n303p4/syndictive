"""MangaDex plugin."""

from datetime import datetime, timedelta
import re

import mangadex as mangadex_wrapper

import syndictive

REGEX_UUID = r"[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}"


@syndictive.library_plugin(
    match_urls=[
        r"https://mangadex\.org\/title\/" + REGEX_UUID + r".*"
    ]
)
class mangadex(syndictive.RSSElementGetters):
    """MangaDex plugin."""

    _api = mangadex_wrapper.Api()
    _scanlation_groups = {}

    @classmethod
    def get_api(cls, ctx):
        return cls._api

    def setup(self):
        self.language = self.config.get("mangadex_language", "en")
        if "mangadex_language" not in self.config:
            self.logger.warning("mangadex_language was not set; defaulting to en")
        self.manga_id = re.findall(REGEX_UUID, self.source_url)
        if not self.manga_id:
            raise ValueError(f"Invalid mangadex source URL {self.source_url} : No UUID was found.")
        self.manga_id = self.manga_id[0]
        self.manga = self.api.view_manga_by_id(self.manga_id)

    @property
    def _default_title(self):
        return f"No title was found for language {self.language}"

    def title(self):
        if not self.manga.title:
            return self._default_title
        return self.manga.title.get(self.language, self.manga.title.get("ja", self._default_title))

    @property
    def _default_description(self):
        return f"No description was found for language {self.language}"

    def description(self):
        if not self.manga.description:
            return self._default_description
        return self.manga.description.get(self.language, self._default_description)

    def preview_url(self):
        return self.api.get_cover(self.manga.coverId).fetch_cover_image()

    def items(self):
        now = datetime.utcnow()
        one_month_ago = now - timedelta(days=28)
        old_items = self.api.manga_feed(self.manga_id, translatedLanguage=[self.language])
        new_items = self.api.manga_feed(
            self.manga_id,
            translatedLanguage=[self.language],
            createdAtSince=one_month_ago.strftime("%Y-%m-%dT%H:%M:%S")
        )
        for chapter in new_items:
            if chapter in old_items:
                old_items.remove(chapter)
        return sorted(
            old_items + new_items,
            key=lambda i: i.publishAt,
            reverse=True
        )

    def item_attributes(self, source_item):
        if source_item.title and source_item.chapter:
            item_title = f"Chapter {source_item.chapter}: {source_item.title}"
        else:
            item_title = str(source_item.chapter) or source_item.id
        if source_item.scanlation_group_id not in self._scanlation_groups:
            try:
                self._scanlation_groups[source_item.scanlation_group_id] = \
                    self.api.scanlation_group_list(group_ids=[source_item.scanlation_group_id])[0]
            except Exception:
                pass
        scanlation_group_name = None
        if source_item.scanlation_group_id in self._scanlation_groups:
            try:
                scanlation_group_name = self._scanlation_groups[source_item.scanlation_group_id].name
            except Exception:
                pass
        return syndictive.FeedItem(
            title=item_title,
            link=f"https://mangadex.org/chapter/{source_item.id}",
            publication_date=source_item.publishAt,
            author=scanlation_group_name
        )

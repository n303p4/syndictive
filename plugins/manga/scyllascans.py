"""Scylla scans."""

from urllib.parse import urlparse, urlunparse, urljoin

import syndictive


@syndictive.soup_plugin(
    match_urls=[r"https://scyllascans.org/work/.*"]
)
class scyllascans(syndictive.RSSElementGetters):
    """Load a Scylla Scans page and convert it to RSS."""

    def setup(self):
        parsed_url = urlparse(self.source_url)
        self.base_url = urlunparse((parsed_url.scheme, parsed_url.netloc, "", "", "", ""))
        self.base_url_read = urljoin(self.base_url, "read")
        self.metadata = {}
        for meta in self.api.find_all("meta"):
            name = meta.get("property") or meta.get("name")
            content = meta.get("content")
            if name and content:
                self.metadata[name] = content

    def title(self):
        return self.metadata.get("og:title")

    def description(self):
        return self.metadata.get("og:description")

    def preview_url(self):
        return self.metadata.get("og:image")

    def items(self):
        raw_anchors = self.api.find_all("a")
        merged_anchors = {}
        for anchor in raw_anchors:
            anchor_href = urljoin(self.base_url, anchor.get("href"))
            if not anchor_href.startswith(self.base_url_read):
                continue
            if anchor_href not in merged_anchors:
                merged_anchors[anchor_href] = syndictive.FeedItem(link=anchor_href)
            chapter_title = anchor.select_one("[class*=ChapterTitle]")
            if chapter_title:
                merged_anchors[anchor_href].title = chapter_title.text.strip()
            last_update = anchor.select_one("[class*=ChapterLastUpdate]")
            if last_update:
                merged_anchors[anchor_href].publication_date = syndictive.to_datetime(last_update.text)
        return merged_anchors.values()

    def item_attributes(self, source_item):
        return source_item

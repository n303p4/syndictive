"""MangaNato plugin."""

import syndictive


@syndictive.soup_plugin(
    match_selectors=[
        "div.story-info-right > h1",
        "div.panel-story-info-description",
        "ul.row-content-chapter",
        "span.info-image > img"
    ]
)
class manganato(syndictive.RSSElementGetters):
    """Load a MangaNato page and convert it to RSS."""

    def setup(self):
        author_string = []
        tags = self.api.select("table.variations-tableInfo tr")
        for tag in tags:
            tag_text = tag.text.lower()
            if not "author" in tag_text:
                continue
            author_tags = tag.select("a")
            for author_tag in author_tags:
                try:
                    author = author_tag.text.strip()
                except Exception:
                    continue
                if author not in author_string:
                    author_string.append(author)
        self.author = ", ".join(author_string) or None

    def title(self):
        return self.api.select_one("div.story-info-right > h1").text

    def description(self):
        return self.api.select_one("div.panel-story-info-description").text

    def preview_url(self):
        return self.api.select_one("span.info-image > img").get("src")

    def items(self):
        return self.api.select_one("ul.row-content-chapter").find_all("li")

    def item_attributes(self, source_item):
        anchor = source_item.select_one("a")
        item_title = anchor.text
        timestring = source_item.select_one("span.chapter-time").get("title")
        item_pubDate = syndictive.to_datetime(timestring)
        return syndictive.FeedItem(
            title=item_title,
            link=anchor.get("href"),
            publication_date=item_pubDate,
            author=self.author
        )

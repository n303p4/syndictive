"""Bato.to plugin."""

import syndictive


@syndictive.soup_plugin(match_urls=[r"https://bato.to/series/[0-9]*.*"])
class batoto(syndictive.RSSElementGetters):
    """MangaPlus plugin."""

    def setup(self):
        attr_items = self.api.select("div.attr-item")
        self.authors = []
        for item in attr_items:
            if "author" not in item.text.lower() and "artist" not in item.text.lower():
                continue
            authors = item.find_all("span")
            for author in authors:
                self.authors.append(author.text.strip())
        self.author_string = ", ".join(self.authors)

    def title(self):
        return self.api.select_one("h3.item-title").text

    def description(self):
        return self.api.select_one("h5.mt-3 + div.mt-3").text

    def preview_url(self):
        return self.api.select_one("img.shadow-6").get("src")

    def items(self):
        return self.api.select("div.episode-list div.item")

    def item_attributes(self, source_item):
        anchor = source_item.find("a")
        chapter_link = anchor.get("href")
        chapter_title = anchor.text
        timestring = source_item.select_one("i.ps-3").text
        chapter_pubDate = syndictive.to_datetime(timestring)
        return syndictive.FeedItem(
            title=chapter_title,
            link=chapter_link,
            publication_date=chapter_pubDate,
            author=self.author_string
        )

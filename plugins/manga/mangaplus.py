"""MangaPlus plugin."""

from datetime import datetime

import syndictive


@syndictive.soup_plugin(
    match_urls=[r"https://mangaplus.shueisha.co.jp/titles/.*"],
    browser=True
)
class mangaplus(syndictive.RSSElementGetters):
    """MangaPlus plugin."""

    def setup(self):
        self.author = self.api.select_one("p[class^=TitleDetailHeader-module_author_]").text

    def title(self):
        return self.api.select_one("h1[class^=TitleDetailHeader-module_title_]").text

    def description(self):
        return self.api.select_one("p[class^=TitleDetailHeader-module_overview_]").text

    def preview_url(self):
        return self.api.select_one("img[class^=TitleDetailHeader-module_coverImage_]").get("src")

    def items(self):
        return self.api.select("div[class^=ChapterListItem-module_chapterListItem_]")

    def item_attributes(self, source_item):
        chapter_title = source_item.select_one("p[class^=ChapterListItem-module_name_]").text
        chapter_title += ": " + source_item.select_one("p[class^=ChapterListItem-module_title_]").text
        chapter_link = source_item.find("a").get("href").replace("comments", "viewer")
        timestring = source_item.select_one("p[class^=ChapterListItem-module_date_]").text
        chapter_pubDate = datetime.strptime(timestring, r"%b %d, %Y")
        return syndictive.FeedItem(
            title=chapter_title,
            link=chapter_link,
            publication_date=chapter_pubDate,
            author=self.author
        )

"""Test plugins for the purpose of testing."""

import rfeed

import syndictive


@syndictive.simple_plugin(
    match_none=True,
    notes="This plugin only exists to test the SimplePlugin class."
)
class test_plugin(syndictive.SimpleGetters):
    """Test plugin."""

    def get_feed(self):
        items = []
        for title in self.plugin.match_urls + self.plugin.match_selectors:
            items.append(rfeed.Item(title=title))
        items.append(rfeed.Item(title=self.plugin.user_agent))
        return rfeed.Feed(title=self.plugin.name, link=self.ctx.source_url,
                          description=self.plugin.notes or "No plugin notes were found",
                          items=items)

    def get_preview(self):
        return None


@syndictive.json_plugin(
    match_urls=[(
        r"https://en.wikipedia.org/w/api.php\?action=query&origin=\*&"
        r"format=json&generator=search&gsrnamespace=0&gsrlimit=5&gsrsearch=.*"
    )],
    notes="This plugin only exists to test the JSONPlugin class."
)
class test_json_plugin(syndictive.RSSElementGetters):
    """Test plugin."""

    def title(self):
        return "RSS"

    def description(self):
        return "RSS"

    def preview_url(self):
        return None

    def items(self):
        return self.api["query"]["pages"].items()

    def item_attributes(self, source_item):
        return syndictive.FeedItem(
            title=source_item[1]["title"],
            link=f"https://{source_item[0]}"
        )

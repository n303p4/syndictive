"""xkcd What If? plugin"""

from datetime import datetime

import syndictive


@syndictive.soup_plugin(match_urls=["https://what-if.xkcd.com/archive"])
class xkcd_what_if(syndictive.RSSElementGetters):
    """xkcd What If? plugin"""

    item_limit = 20

    def title(self):
        return self.api.select_one("#whatif-title").text

    def description(self):
        return self.api.select_one("#intro-text").text

    def preview_url(self):
        return self.api.select_one("img[alt='what if illustration']").get("src")

    def items(self):
        return list(reversed(self.api.select(".archive-entry")))

    def item_attributes(self, source_item):
        image_element = source_item.select_one(".archive-image")
        title_element = source_item.select_one(".archive-title")
        return syndictive.FeedItem(
            title=title_element.text,
            link=title_element.find("a").get("href"),
            publication_date=datetime.strptime(source_item.select_one(".archive-date").text, "%B %d, %Y"),
            image=image_element.get("src")
        )

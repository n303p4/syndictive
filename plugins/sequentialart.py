"""Jolly Jack's Sequential Art."""

import syndictive


@syndictive.soup_plugin(match_none=True)
class _sequentialart_dates(syndictive.RSSElementGetters):
    """Additional plugin to fetch dates."""

    item_limit = 20

    def title(self):
        return None

    def description(self):
        return None

    def preview_url(self):
        return None

    def items(self):
        return list(strong.text for strong in self.api.select("#wb_Text1 strong"))

    def item_attributes(self, source_item):
        publication_date = syndictive.to_datetime(source_item)
        if not publication_date:
            return None
        return syndictive.FeedItem(
            title=source_item,
            publication_date=publication_date
        )


@syndictive.soup_plugin(match_urls=[r"https://www.collectedcurios.com/sequentialart.php.*"])
class sequentialart(syndictive.RSSElementGetters):
    """Plugin for Jolly Jack's Sequential Art."""

    item_limit = 20

    def setup(self):
        self.context.rss = True
        context = syndictive.PluginContext(
            source_url="https://www.collectedcurios.com/index.html",
            config=self.config,
            preview_requested=False
        )
        self.sequentialart_dates = _sequentialart_dates.run(context)

    def title(self):
        return self.api.find("title").text

    def description(self):
        return self.title()

    def preview_url(self):
        return self.api.select_one("img#logo").get("src")

    def items(self):
        previous_comic_url = self.api.select_one("a#backOne").get("href")
        base_comic_url, previous_comic_id = previous_comic_url.split("=")
        previous_comic_id = int(previous_comic_id)
        items = []
        for comic_id in range(previous_comic_id+1, previous_comic_id-19, -1):
            items.append(syndictive.FeedItem(
                title=f"{comic_id}",
                link=f"{base_comic_url}={comic_id}",
                image=f"/SA_{comic_id}_small.jpg"
            ))
        for item, date_item in zip(items, self.sequentialart_dates.items):
            try:
                item.publication_date = date_item.publication_date
            except Exception:
                continue
        return items

    def item_attributes(self, source_item):
        return source_item

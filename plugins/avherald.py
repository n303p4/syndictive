"""The Aviation Herald."""

import syndictive


@syndictive.soup_plugin(match_urls=["https://avherald.com", "https://avherald.com/"])
class avherald(syndictive.RSSElementGetters):
    """Plugin for avherald.com."""

    item_limit = 50

    def setup(self):
        self.do_not_follow = self.config.get("avherald_filter", [])
        self.running_date = None

    def title(self):
        return self.api.find("title").text

    def description(self):
        return self.api.select_one("meta[name='description']").get("content")

    def preview_url(self):
        return self.api.select_one("link[rel='apple-touch-icon']").get("href")

    def items(self):
        return list(self.api.select("table[width='100%']"))

    def item_attributes(self, source_item):
        running_date_candidate = source_item.select_one(".bheadline_avherald")
        if running_date_candidate:
            self.running_date = syndictive.to_datetime(running_date_candidate.text)
            return
        event_type_img = source_item.find("img")
        if source_item.find("table") or not event_type_img:
            return
        event_type = event_type_img.get("title")
        if event_type == "Next":
            return
        if event_type in self.do_not_follow:
            return
        anchor = source_item.find("a")
        item_title = f"{event_type}: {anchor.text}"
        item_link = anchor.get("href")
        return syndictive.FeedItem(
            title=item_title,
            link=item_link,
            publication_date=self.running_date
        )

"""Plugin that forwards existing Atom feeds to Syndictive."""

import syndictive


@syndictive.forwarding_plugin(
    match_selectors=['link[type="application/atom+xml"] || a[href*="/atom"] || feed']
)
class atom_forwarding(syndictive.ForwardingGetters):
    """Generic Atom forwarding plugin. Try to detect Atom feeds on the page itself and copy them as-is.

    You can also add an Atom URL directly.
    """

    def feed_url(self):
        if self.api.find("feed"):
            return self.source_url
        for selector_group in self.plugin.match_selectors:
            for selector in selector_group.split("||"):
                self.logger.debug(f"Checking selector {selector}")
                element = self.api.select_one(selector.strip())
                if element:
                    return element.get("href")

    def preview_url(self):
        return None

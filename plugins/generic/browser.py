"""Generic plugin for Syndictive that uses an entire browser engine.

It is *not* recommended to use this plugin if you can help it.

However, it may be necessary for certain types of sites.
"""

import helium

import syndictive


@syndictive.browser_plugin()
class generic_browser(syndictive.RSSElementGetters):

    item_limit = 100

    def title(self):
        return self.api.title

    def description(self):
        return self.source_url

    def preview_url(self):
        return helium.S("img").web_element.get_attribute("src")

    def items(self):
        anchors = []
        anchor_predicates = helium.S("article a"), helium.S("li a")
        for predicate in anchor_predicates:
            try:
                helium.wait_until(predicate.exists, timeout=4)
                anchors += list(helium.find_all(predicate))
            except Exception:
                pass
            if len(anchors) > 1:
                return anchors
        return anchors

    def item_attributes(self, source_item):
        anchor_element = source_item.web_element
        return syndictive.FeedItem(
            title=anchor_element.get_attribute("innerText"),
            link=anchor_element.get_attribute("href")
        )

"""Plugin that forwards existing RSS feeds to Syndictive."""

import syndictive


@syndictive.forwarding_plugin(
    match_selectors=['link[type="application/rss+xml"] || a[href*="/rss"] || a[href*=".xml"] || rss']
)
class generic_forwarding(syndictive.ForwardingGetters):
    """Generic RSS forwarding plugin. Try to detect RSS feeds on the page itself and copy them as-is.

    You can also add an RSS URL directly.
    """

    def setup(self):
        """Set RSS mode instead of Atom mode."""
        self.context.rss = True

    def feed_url(self):
        if self.api.find("rss"):
            return self.source_url
        for selector_group in self.plugin.match_selectors:
            for selector in selector_group.split("||"):
                self.logger.debug(f"Checking selector {selector}")
                element = self.api.select_one(selector.strip())
                if element:
                    return element.get("href")

    def preview_url(self):
        return None

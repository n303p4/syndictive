"""Generic plugin for Syndictive that aims to work on a range of sites."""

from datetime import datetime

import syndictive


@syndictive.soup_plugin(match_selectors=["article || li || tr"])
class generic_soup(syndictive.RSSElementGetters):
    """Try to load a generic page and convert it to RSS."""

    item_limit = 100

    def setup(self):
        self.metadata = {}
        for meta in self.api.find_all("meta"):
            name = meta.get("property") or meta.get("name")
            content = meta.get("content")
            if name and content:
                self.metadata[name] = content

    def title(self):
        for prefix in "og:", "twitter:":
            content = self.metadata.get(f"{prefix}title")
            if content:
                return content
        return self.api.find("title").text

    def description(self):
        for prefix in "", "og:", "twitter:":
            content = self.metadata.get(f"{prefix}description")
            if content:
                return content
        return self.source_url

    def preview_url(self):
        for prefix in "og:", "twitter:":
            content = self.metadata.get(f"{prefix}image")
            if content:
                return content
        return self.api.find("img").get("src")

    def items(self):
        articles = list(self.api.select("article"))
        if len(articles) > 1:
            return articles
        return articles + \
               list(self.api.select("li")) + \
               list(self.api.select("tr"))

    def item_attributes(self, source_item):
        return_dict = {}
        anchor = source_item.find("a")
        if not anchor or not anchor.text:
            return
        item_link = anchor.get("href")
        if not item_link:
            return
        if item_link.rstrip("/") in self.source_url:
            return
        return_dict["title"] = anchor.text
        return_dict["link"] = item_link
        item_pubDate = None
        time = source_item.find("time")
        if time:
            item_pubDate = syndictive.to_datetime(time.get("datetime"))
        if not item_pubDate:
            for descendant in source_item.descendants:
                if not hasattr(descendant, "attrs"):
                    continue
                for timestring in list(descendant.attrs.values()) + [descendant.text]:
                    item_pubDate = syndictive.to_datetime(timestring)
                    if item_pubDate:
                        break
                if item_pubDate:
                    break
        if item_pubDate:
            return_dict["pubDate"] = item_pubDate
        else:
            return_dict["pubDate"] = datetime(year=1970, month=1, day=1)
        return syndictive.FeedItem(
            title=return_dict.get("title"),
            link=return_dict.get("link"),
            publication_date=return_dict.get("pubDate")
        )
